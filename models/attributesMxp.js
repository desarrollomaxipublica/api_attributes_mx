'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  autoIncrement = require('mongoose-auto-increment'),
  database = require('../database')


var attributesSchema = new Schema({
  _id: {
    type: Number
  },
  oldId: {
    type: String
  },
  id: {
    type: String,
    required: true,
    unique: true
  },
  categoryId: {
    type: String,
    required: true
  },
  category: {
    type: String,
    required: true
  },
  attributes: [{
    _id: false,
    id: {
      type: String,
      required: true
    },
    oldId: {
      type: String,
      required: false
    },
    label: {
      type: String,
      required: false
    },
    help: {
      type: String,
      required: false
    },
    alert: {
      type: String,
      required: false
    },
    placeholder: {
      type: String,
      required: false
    },
    type: {
      type: String,
      required: true
    },
    inputType: {
      type: String,
      required: true
    },
    valueType: {
      type: String,
      required: true
    },
    required: {
      type: Boolean,
      required: true
    },
    groupId: {
      type: String,
      required: true
    },
    allowEdit: {
      type: Boolean,
      required: true
    },
    order: {
      type: Number,
      required: true
    },
    value: {
      type: String,
      required: false
    },
    valueId: {
      type: String,
      required: false
    },
    rules: {
      type: String,
      required: false
    },values: [{
      required: false,
      id: {
        type: String,
        required: false
      },
      name: {
        type: String,
        required: false
      },
      _id: false,
      id: {
        type: String,
        required: false
      },
      oldId: {
        type: String,
        required: false
      },
      label: {
        type: String,
        required: false
      },
      help: {
        type: String,
        required: false
      },
      alert: {
        type: String,
        required: false
      },
      placeholder: {
        type: String,
        required: false
      },
      type: {
        type: String,
        required: false
      },
      inputType: {
        type: String,
        required: false
      },
      valueType: {
        type: String,
        required: false
      },
      required: {
        type: Boolean,
        required: false
      },
      groupId: {
        type: String,
        required: false
      },
      allowEdit: {
        type: Boolean,
        required: false
      },
      order: {
        type: Number,
        required: false
      },
      value: {
        type: String,
        required: false
      },
      valueId: {
        type: String,
        required: false
      },
      values: [{
        _id: false,
        required: false,
        id: {
          type: String,
          required: false
        },
        name: {
          type: String,
          required: false
        }
      }]
    }]
  }]
})

var attributesS = mongoose.model('attributesMxp', attributesSchema, 'attributesMxp');
attributesSchema.plugin(autoIncrement.plugin, {
  model: 'attributesS',
  field: '_id',
  startAt: 0
});
module.exports = attributesS;