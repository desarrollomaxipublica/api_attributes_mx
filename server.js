"use strict";

var express =  require('express'),
  app     =  express(),
  http   =  require("http"),
  https   =  require("https"),
  server = http.createServer(app),
  database = require('./database'),
  routes  = require("./routes/routes"),
  mongoose = require("mongoose"),
  port = 3004,
  bodyParser = require('body-parser')
  app.use('/health', (req, res) => {
    res
      .status(200)
      .send({
        status: 200,
        name: 'ok',
        message: 'i\'m healthy',
        customMessage: 'Aqui estoy'
      })
  })
  app.use(bodyParser.urlencoded({extended:false}));
  app.use(bodyParser.json());
  app.use(bodyParser.json({limit: '50mb'}));
  app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
  app.use('/check/health', (req, res) => {
    res.send({
      name: 'api_mxp_quiestion_to_messages',
      status: 'ok'
    })
  })
  app.use('/', express.static('public'));
  app.use('/', routes);
  

  server.listen(port, function() {
    console.log("Node server running on http://localhost:"+port )

})
