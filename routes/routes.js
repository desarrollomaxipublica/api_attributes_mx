'use strict';

const express = require('express'),
	router = express(),
	api = require('../controllers/api'),
	post = require('../controllers/post'),
	get = require('../controllers/get'),
	getReq = require('../controllers/getRequired'),
	getMas = require('../controllers/getMaster'),
	put = require('../controllers/put'),
	del = require('../controllers/delete'),
	sam = require('../controllers/sam/getSam')

const token = require('../controllers/utils/token')

router.use(configHeader)

router.post('/',
	// api.validTokenMxp,
	configHeader,
	post.validateData,
	post.attributesGroup,
	post.completeData,
	post.sortData,
	post.saveAttributes)

	.get('/mx/:attributeId',
		api.validTokenMxp,
		configHeader,
		api.getDealer,
		get.findPrice,
		get.findAttribute,
		get.loadDeafault,
		get.getJson,
		get.loadValuesDefault,
		get.buildAttrbutesExtra,
		get.viewJson)

	.get('/sam/:attributeId',
		sam.validateToken,
		configHeader,
		api.getDealer,
		sam.findAttr,
		sam.getTemplateSam,
		sam.makeNewAtt,
		sam.armingJSON)

	.get('/valid/mx/:attributeId',
		configHeader,
		api.validTokenMxp,
		api.getDealer,
		getReq.findAttribute,
		getReq.loadDeafault,
		getReq.getJson,
		getReq.viewJson)

	.get('/attributes/mx/:attributeId',
		configHeader,
		api.validTokenMxp,
		api.getDealer,
		getMas.findAttribute,
		getMas.loadDeafault,
		getMas.getJson,
		getMas.loadValuesDefault,
		getMas.buildAttrbutesExtra,
		getMas.viewJson)

	.put('/:attributeId',
		configHeader,
		api.validTokenMxp,
		put.validateData,
		post.validateAtributes,
		put.updateAttribute)

	.delete('/:attributeId',
		configHeader,
		del.deleteAttribute)

	.use(function (err, req, res, next) {
		console.error(err.stack);
		res.status(500).send({
			status: 500,
			name: 'internalServerError',
			message: 'Internal Server Error' + (err.message ? ': ' + err.message : ''),
			customMessage: 'Error Interno en el Servidor'
		});
	})

	.use((req, res) => {

		return res
			.status(404)
			.send({
				status: 404,
				name: "pageNotFound",
				message: 'Resurce not found',
				customMessage: 'El recurso solicitado no se encuentra'
			})
	})

function configHeader(req, res, next) {

	res.setHeader('Access-Control-Allow-Origin', '*')
	next()
}







module.exports = router;