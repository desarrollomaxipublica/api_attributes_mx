'use strict'
var dbAttributes = require('../models/attributesMxp')


function validateData(req, res, next) {

	let mIndices = ['category', 'attributes']
	let notFound = []
	mIndices.forEach((e) => {
		if (!req.body[e]) {
			console.log('valido esto' + e)
			notFound.push(e)
		}
	})



	if (notFound.length !== 0) {

		res.send({
			status: 200,
			messague: 'Not Found : ' + notFound,
			notFound: notFound
		})
	} else {
		if (req.body.attributes.length === 0) {
			res.send({
				status: 200,
				messague: 'Debe de haber haber 1 o mas atributos',
				notFound: 'attributes'
			})
		} else {
			if (req.body.id)
				res.send({
					status: 200,
					messague: 'id can not be modified',
					notFound: 'id'
				})
			if (req.body.categoryId)
				res.send({
					status: 200,
					messague: 'categoryId can not be modified',
					notFound: 'categoryId'
				})
			else
				next()
		}
	}
}



function updateAttribute(req, res, next) {
	console.log('aqui', '1', req.body)
	dbAttributes.findOneAndUpdate({
		id: req.params.attributeId
	}, {
		$set: req.body
	}, (err, doc) => {
		console.log('que pedo', err, doc)
		if (!err) {
			if (doc) {
				console.log('aqui')
				res.send(doc).status(200)
			} else {
				res.send({
					status: 200,
					messague: 'The attribute with id ' + req.params.attributeId + ' not exist'
				}).status(200)
			}
		} else {
			res.send({
				'status': 500,
				messague: err
			}).status(500)
		}
	})
}

module.exports.validateData = validateData
module.exports.updateAttribute = updateAttribute