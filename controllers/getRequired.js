'use strict'
var async = require('async'),
	dbAttributes = require('../models/attributesMxp'),
	request = require('request'),
	base64url = require('base64url')


function validTokenMxp(req, res, next) {

	console.log('validando token para get')
	if (req.query.access_token || req.query.token) {
		console.log('solicitando token')
		if (req.query.access_token) {
			request.get({
				url: 'http://184.72.65.48:3010/' + req.query.access_token
			}, (error, respon, body) => {

				console.log(body)
				var valid_token = JSON.parse(body);
				var dealer;
				var type_dealer;
				var datos = valid_token.mensaje.split("-");
				console.log(datos.length);
				if (datos.length > 3) {
					console.log('token valido')
					next()
				} else {
					console.log(valid_token.mensaje.indexOf("Ocurrio un error"));
					if (valid_token.mensaje.indexOf("Ocurrio un error") != -1) {
						res.status(400);
						res.send({
							status: 400,
							mensaje: "access_token no valid"
						});
						return 0;
					} else {
						res.status(400);
						res.send({
							status: 400,
							mensaje: valid_token.mensaje
						});
						return 0;
					}
				}
			})
		} else if (req.query.token) {
			let url = 'http://d17.maxipublica.com/oauth/valid/' + req.query.token
			console.log(url)
			request.get({
				url: url
			}, (err, response, body) => {
				console.log(response.statusCode)
				if (!err) {
					if (response.statusCode == 200) {
						req.token = {
							'sellerId': base64url.decode(body.sellerId)
						}
						next()
					} else {
						res
							.status(response.statusCode)
							.send(JSON.parse(body))
					}
				} else {
					res
						.status(500)
						.send({
							status: 500,
							name: 'internalError',
							message: 'Ocurrio un error interno'
						})
				}
			})
		} else {
			res
				.status(400)
				.send({
					status: 400,
					name: 'notAccesToken',
					message: 'The access_token is required'
				})
		}
	} else {
		res
			.status(400)
			.send({
				status: 400,
				name: 'notAccesToken',
				message: 'The access_token is required'
			})
	}
}


function findAttribute(req, res, next) {
	
	dbAttributes.findOne({
		id: req.params.attributeId
	}, (err, doc) => {
		// console.log('attribbutt econtrado',err, doc, req.params.attributeId)
		if (!err) {
			if (doc) {
				req.data = doc
				delete req.data.__v
				next()
			} else {
				next()
			}
		} else {
			res
				.status(500)
				.send({
					'status': 500,
					message: err
				}).status(500)
		}
	})
}

function loadDeafault(req, res, next) {
	//Carga la plantilla por default en caso de no encontrar la categoria identificada
	console.log('cargando por default')

	let def = 'v_1'
	if (/v_1/.test(req.params.attributeId)) {
		console.log('viene de autos')
		def = 'v_1'
	} else if (/v_2/.test(req.params.attributeId)) {
		console.log('viene de camiones')
		def = 'v_2'
	}


	if (!req.data) {
		console.log('buscare')
		dbAttributes.findOne({
			id: def
		}, (err, doc) => {
			if (!err) {
				if (doc) {
					console.log('encontre')
					req.data = doc
					delete req.data.__v
					next()
				} else {
					console.log('no encontre')
					res
						.status(409)
						.send({
							status: 409,
							message: 'The attribute with id ' + req.params.attributeId + ' not exist'
						}).status(200)
				}
			} else {
				res
					.status(500)
					.send({
						'status': 500,
						message: err
					}).status(500)
			}
		})
	} else {
		next()
	}
}

function getJson(req, res, next) {
	//Acomodamos las salidas  solo para los registros que existan y con el campo value

	let newAtribute = []
	let attribute = {}

	if (req.seller.attributesExtra && req.seller.attributesExtra.groupID && req.seller.attributesExtra.groupID == 7) {
		console.log(`ODOMETER SUFFIX BY GROUP ${req.seller.attributesExtra.groupID} `)
		newAtribute.push('odometerSuffix')
	}
	req.data.attributes.forEach((e) => {
		if (e.valueType == 'group') {

			e.values.forEach((b) => {
				if (req.seller.attributesRequired.indexOf(b.id) >= 0) {
					b['required'] = true
				}
				if (b.required) {
					console.log(b.id)
					newAtribute.push(b.id)
				}
			})
		} else {
			if (req.seller.attributesRequired.indexOf(e.id) >= 0) {
				e['required'] = true
			}
			if (e.required) {
				console.log(e.id)
				newAtribute.push(e.id)
			}

		}

	})
	req.data.attributes = newAtribute
	//res.send(newAtribute)
	let data = {
		id: req.data.id,
		categoryId: req.data.categoryId,
		category: req.data.category,
		attributes: newAtribute
	}
	req.data = null
	req.data = data
	next()
}


function viewJson(req, res, next) {
	res
		.status(200)
		.send(req.data).status(200)
}


module.exports.validTokenMxp = validTokenMxp
module.exports.findAttribute = findAttribute
module.exports.loadDeafault = loadDeafault
module.exports.getJson = getJson
module.exports.viewJson = viewJson