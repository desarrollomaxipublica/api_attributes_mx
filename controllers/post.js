'use strict'
var async = require('async'),
	dbAttributes = require('../models/attributesMxp'),
	c = require('./services/v_1')


function validateData(req, res, next){
	req.body.userId = 7
	//Verificamos estos datos principales
	let mIndices = ['id', 'categoryId', 'category', 'attributes']
	let notFound = []
	mIndices.forEach((e)=> {
		if (!req.body[e]) {
			console.log('valido esto'+e)
			notFound.push(e)
		}
	})

	if (notFound.length !== 0) {

		res
			.status(400)
			.send({status : 400, messague : 'Not Found : ' + notFound, notFound : notFound})
	}else{
		if (req.body.attributes.length === 0) {
			res.send({status : 200, messague : 'Debe de haber haber 1 o mas atributos', notFound : 'attributes'})
		}else{
			req.body.id = req.body.id.replace(/\s/g,'')
			req.body.categoryId = req.body.categoryId.replace(/\s/g,'')
			console.log('vienen todos los datos Principales')
			if (/v_2/.test(req.body.categoryId)) {
				mapAtt = require('./services/v_2')
			}
			next()
		}
	}
}


function attributesGroup(req, res, next){
	let notFound = []
	req.ids = []
	let order = 0
	req.body.attributes.forEach((e)=>{
		console.log(e.id)
		e['order'] = mapAtt[e.id].order

		if (e.valueType == 'group') {
			console.log(e.id)
			e.values.forEach((b)=>{
				b['order'] = mapAtt[b.id].order
				if (mapAtt[b.id]) {
					b['label'] = mapAtt[b.id].label
					b['help'] = mapAtt[b.id].help
					b['alert'] = mapAtt[b.id].alert
					b['placeholder'] = mapAtt[b.id].placeholder
					
				}else{
					b['label'] = ''
					b['help'] = ''
					b['alert'] = ''
					b['placeholder'] = ''
				}

				notFound = validateAttribute(b, notFound)
				console.log(notFound)
				req.ids.push(b.id)
			})
		}else{
			if (mapAtt[e.id]) {
					e['label'] = mapAtt[e.id].label
					e['help'] = mapAtt[e.id].help
					e['alert'] = mapAtt[e.id].alert
					e['placeholder'] = mapAtt[e.id].placeholder
					
				}else{
					e['label'] = ''
					e['help'] = ''
					e['alert'] = ''
					e['placeholder'] = ''
				}
				req.ids.push(e.id)
			notFound = validateAttribute(e, notFound)
		}
		order += 1
	})

	console.log('termine de validar attributes')
	if (notFound.length !== 0) {
		res.send({status : 400, message : '' + notFound, notFound : notFound})
	}else{
		//res.send(req.body)
		next()

		// res.send(req.ids)
	}
}

function validateAtributes(req, res, next){

	let mIndices = [
			{id :'id',  type : 'string'},
			{id :'oldId', type : 'string'},
			{id : 'inputType', type : 'string'},
			{id : 'valueType', type : 'string'},
			{id :'type', type : 'string'},
			{id :'groupId', type : 'string'}	
		]
	let notFound = []



	async.each(req.body.attributes,(attribute, callback)=> {

		//console.log(typeof attribute.allowEdit)
		mIndices.forEach((e)=> {
			if (!attribute[e.id]) {
				console.log('valido esto'+e.id, attribute[e.id])
				notFound.push('attribute >'+ attribute.id+', field ' + e.id +' is required' )

			}else if (attribute[e.id]){
				if (attribute[e.id] && typeof  attribute[e.id] !== e.type) {
					notFound.push('attribute >'+ attribute.id+', field '+attribute[e.id]+' must be ' + e.type )
				}else{
					attribute[e.id] = attribute[e.id].trim()
				}
				

			}
		})


		if (!attribute.allowEdit) {
			attribute.allowEdit = false;
		}else if (!(typeof attribute.allowEdit === 'boolean')){
			notFound.push('attribute >'+ attribute.id+', field allowEdit must be boolean')
		}
		
		if (!attribute.required) {
			attribute.required = false;
		}else if (!(typeof attribute.required === 'boolean')){
			notFound.push('attribute >'+ attribute.id+', field required must be boolean')
		}

		// if (!attribute.order) {
		// 	notFound.push('attribute >'+ attribute.id+', field ' + attribute.order +' is required' )
		// }else{
		// 	if (!(typeof attribute.order === 'string' || typeof attribute.order === 'number' )) {
		// 		notFound.push('attribute >'+ attribute.id+', field order must be string or number')
		// 	}
		// }

		if (attribute.value) {

			if (!(typeof attribute.value === 'string' || typeof attribute.value === 'number' || typeof attribute.value === 'boolean' )) {
				notFound.push('attribute >'+ attribute.id+', field value must be string, boolean or number')
			}
			console.log('ya tiene valor')
		}else if (attribute.values ) {
				if (typeof attribute.values == 'object') {
					if (attribute.values.length > 0) {

						attribute.values.forEach((e)=>{

							if (!e.id) {
								notFound.push('the field id is required in values, attribute ' + attribute.id )
							}else{
								e.id = e.id.replace(/\s/g,'')
							}
							if(!e.name){
								notFound.push('the field name is required in values, attribute ' + attribute.id )
							}else{

							}

						})

						if (!attribute.valueId) {
							attribute.valueId = ""
						}

					}else{
						notFound.push('attribute >'+ attribute.id+', field values is required')
					}
				}else{
					notFound.push('attribute >'+ attribute.id+', field values must be object')
				}
				
		}else{
			if (typeof attribute.value !=='boolean'  && typeof attribute.value !== 'string' ) {
				console.log('el campo es boolean value' + typeof attribute.value)
				notFound.push('attribute >'+ attribute.id+', field value or values is required')
			}
			else{
				console.log('el campo es boolean')
			}
		}

		callback()

	},(err, result) =>{
		console.log('termine de validar attributes')
		if (notFound.length !== 0) {
			res
			.status(400)
				.send({status : 400, messague : '' + notFound, notFound : notFound})
		}else{
			//res.send(req.body)
			next()
		}
	})

}

function validateAttribute(attribute, notFound){

	let mIndices = [
			{id :'id',  type : 'string'},
	//			{id :'idOld', type : 'string'},
	//			{id :'label', type : 'string'},
			{id : 'inputType', type : 'string'},
			{id : 'valueType', type : 'string'},
			{id :'type', type : 'string'},
			{id :'groupId', type : 'string'}	
		]

		mIndices.forEach((e)=> {
			if (!attribute[e.id]) {
				//console.log('valido esto'+e.id, e.type)
				notFound.push('attribute >'+ attribute.id+', field ' + e.id +' is required' )

			}else if (attribute[e.id]){
				if (attribute[e.id] && typeof  attribute[e.id] !== e.type) {
					notFound.push('attribute >'+ attribute.id+', field '+attribute[e.id]+' must be ' + e.type )
				}else{
					attribute[e.id] = attribute[e.id].trim()
				}
				

			}
		})


		if (!attribute.allowEdit) {
			attribute.allowEdit = false;
		}else if (!(typeof attribute.allowEdit === 'boolean')){
			notFound.push('attribute >'+ attribute.id+', field allowEdit must be boolean')
		}
		
		if (!attribute.required) {
			attribute.required = false;
		}else if (!(typeof attribute.required === 'boolean')){
			notFound.push('attribute >'+ attribute.id+', field required must be boolean')
		}

		// if (!attribute.order) {
		// 	notFound.push('attribute >'+ attribute.id+', field ' + attribute.order +' is required' )
		// }else{
		// 	if (!(typeof attribute.order === 'string' || typeof attribute.order === 'number' )) {
		// 		notFound.push('attribute >'+ attribute.id+', field order must be string or number')
		// 	}
		// }

		if (attribute.value) {

			if (!(typeof attribute.value === 'string' || typeof attribute.value === 'number' || typeof attribute.value === 'boolean' )) {
				notFound.push('attribute >'+ attribute.id+', field value must be string, boolean or number')
			}
			//console.log('ya tiene valor')
		}else if (attribute.values ) {
				if (typeof attribute.values == 'object') {
					if (attribute.values.length > 0) {

						attribute.values.forEach((e)=>{

							if (!e.id) {
								notFound.push('the field id is required in values, attribute ' + attribute.id )
							}else{
								e.id = e.id.replace(/\s/g,'')
							}
							if(!e.name){
								notFound.push('the field name is required in values, attribute ' + attribute.id )
							}else{

							}

						})

						if (!attribute.valueId) {
							attribute.valueId = ""
						}

					}else{
						notFound.push('attribute >'+ attribute.id+', field values is required')
					}
				}else{
					notFound.push('attribute >'+ attribute.id+', field values must be object')
				}
				
		}else{
			if (typeof attribute.value !=='boolean'  && typeof attribute.value !== 'string' ) {
				//console.log('el campo es boolean value' + typeof attribute.value)
				notFound.push('attribute >'+ attribute.id+', field value or values is required')
			}
			else{
				console.log('el campo es boolean')
			}
		}

		return notFound
}

function completeData(req, res, next){
	// Los datos que no me envian  los reemplzao por el deafult

	Object.keys(mapAtt).forEach((e)=>{
		if (e !== 'priceGroup' && e!== 'odometerGroup') {
			if (req.ids.indexOf(e) == -1) {
				mapAtt[e]['id'] = e
				req.body.attributes.push(mapAtt[e])
			}
		}
	})

	//res.send(req.body)
	next()

}

function sortData(req, res, next){
	let mjson = []

	Object.keys(mapAtt).forEach((e)=>{

		//console.log('mi key', e)
		req.body.attributes.forEach((b)=>{
			if (e == b.id) {
				mjson.push(b)
			}
		})
	})

	//console.log('=====================', 'ordenada', '==============')
	//console.log(mjson)
	req.body.attributes = mjson
	// res.send(Object.keys(mapAtt))
	next()

}

function saveAttributes(req, res, next){
	//Guardo el atributto en la collecion
		let dbSave  = new dbAttributes(req.body)
		// res.send(req.body)
	dbSave.save((err, doc)=> {

		console.log(err)
		console.log('attribute guardado', err)
		if (err) {
			res
				.status(409)
				.send({'status' : 409, message : err.message })
		}else{
			res
				.status(200)
				.send({'status' : 200, message : 'attributes saved ;)' })
		}
		
	})

}


module.exports.validateData = validateData
module.exports.attributesGroup = attributesGroup
module.exports.validateAtributes = validateAtributes
module.exports.completeData = completeData
module.exports.sortData = sortData
module.exports.saveAttributes = saveAttributes