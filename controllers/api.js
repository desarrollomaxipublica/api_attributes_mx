'use strict'

var request = require('request'),
  base64url = require('base64url')

function validTokenMxp(req, res, next) {
  // req.body.userId = 7 
  console.log(`
╔═══════════════════════════════╗
  A P I    A T T R I B U T E S        
          (> START <)
╚═══════════════════════════════╝`)
  console.log('═══[ OK ]  Validation Token/Access')
  if (req.query.access_token || req.query.token) {
    if (req.query.access_token) {
      console.log('   ╚═══[ Validation by Access Token ]')
      request.get({
        url: 'http://184.72.65.48:3010/' + req.query.access_token
      }, (error, respon, body) => {
        console.log('(*_*)=====> BODY')
        console.log(body)

        var valid_token = JSON.parse(body);
        var dealer;
        var type_dealer;
        var datos = valid_token.mensaje.split("-");
        console.log(datos.length);
        if (datos.length > 3) {
          if (req.body) {
            req.body['userId'] = datos[0]
          } else {
            req.body = {}
            req.body['userId'] = datos[0]
          }
          dealer = datos[0];
          type_dealer = datos[3];
          if (type_dealer != "admin") {
            res.status(400);
            res.send({
              status: 400,
              mensaje: "Access Privileges Error"
            });
            return 0;
          } else {
            console.log('token valido')
            next()
          }
        } else {
          console.log(valid_token.mensaje.indexOf("Ocurrio un error"));
          if (valid_token.mensaje.indexOf("Ocurrio un error") != -1) {
            res.status(400);
            res.send({
              status: 400,
              mensaje: "access_token no valid"
            });
            return 0;
          } else {
            res.status(400);
            res.send({
              status: 400,
              mensaje: valid_token.mensaje
            });
            return 0;
          }
        }
      })
    } else if (req.query.token) {
      console.log('   ╚═══[ Validation by Token ]')
      let url = 'http://d17.maxipublica.com/oauth/valid/' + req.query.token
      // console.log(url)
      request.get({
        url: url
      }, (err, response, body) => {
        if (!err) {
          if (response.statusCode == 200) {
            console.log('      ╚═══[  StatusCode: ' + response.statusCode)
            body = JSON.parse(body)
            req.token = {}
            req.body.userId = base64url.decode(body.userId)
            req.body.userId = base64url.decode(body.userId)
            req.token.sellerId = base64url.decode(body.sellerId)
            req.token.rol = base64url.decode(body.rol)
            req.token.type = base64url.decode(body.type)
            if ((req.token.type === 'admin' && req.token.rol === 'admin') || req.token.type === 'seller' || req.token.type === 'group') {
              next()
            } else {
              res
                .status(401)
                .send({
                  status: 401,
                  name: 'notAllowed',
                  message: 'You do not have permissions',
                  customMessage: 'No puedes acceder a este recurso'
                })
            }
          } else {
            res
              .status(response.statusCode)
              .send(JSON.parse(body))
          }
        } else {
          res
            .status(500)
            .send({
              status: 500,
              name: 'internalError',
              message: 'Ocurrio un error interno',
              customMessage: 'Ocurrio un error interno'
            })
        }
      })
    } else {
      res
        .status(401)
        .send({
          status: 401,
          name: 'notAccesToken',
          message: 'The access_token is required',
          customMessage: 'El token es requerido'
        })
    }
  } else {
    res
      .status(400)
      .send({
        status: 400,
        name: 'notAccesToken',
        message: 'The access_token is required',
        customMessage: 'El token es requerido'
      })
  }
}

function getDealer(req, res, next) {
  console.log('═══[ OK ]  Get Dealer')

  req.categoryId = 'v_1'
  if (/v_2/.test(req.params.attributeId))
    req.categoryId = 'v_2'


  console.log(`   ╚═══[Category ${req.categoryId} ]`)
  let url = 'http://d17.maxipublica.com/sellers/attributes/' +
    (req.query.sellerId || req.token.sellerId) + '?token=' + req.query.token

  console.log(`
╔═══════════════════════════╗
    URL SELLERS ATRIBUTES        
╚═══════════════════════════╝`)
  // console.log(url);


  request.get({
    url: url
  }, (err, response, body) => {
    //console.log(body, req.token.sellerId)
    if (!err) {
      req.attr = []
      body = JSON.parse(body)
      req.sellerSam =body
      req.seller = {
        sellerId: body._id || body.id,
        groupKey: body.groupKey && body.groupKey[0] ? body.groupKey[0] : '',
        attributesRequired: body.attributesRequired && body.attributesRequired[req.categoryId] ? body.attributesRequired[req.categoryId] : [],
        valuesDefault: body.valuesDefault && body.valuesDefault.attributes[req.categoryId] ?
          body.valuesDefault.attributes[req.categoryId] : [],
        attributesExtra: body.valuesDefault && body.valuesDefault.attributesExtra ?
          body.valuesDefault.attributesExtra : undefined,
        badges: body.valuesDefault && body.valuesDefault.badges ? body.valuesDefault.badges : null

      }
      let location = ''

      if (body.address && body.address[0])
        if (body.address[0].location && body.address[0].location.state)
          location = body.address[0].location.state.name

      req.seller['location'] = location
      //console.log(' body.valuesDefault: ', body.valuesDefault);
      req.attr = body.valuesDefault
      req.frontV = body.frontVersion
      req.brandQ = req.query.brand || ''
      //console.log('estos son mis datos requeridos', req.seller)
    } else {
      req.seller = {
        sellerId: '',
        attributesRequired: [],
        valuesDefault: []
      }
    }
    // return res.send(req.sellerSam)
    // return res.send(req.seller)
    next()
  })
}
module.exports.validTokenMxp = validTokenMxp
module.exports.getDealer = getDealer