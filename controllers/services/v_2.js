module.exports = {
  "price": {
    "id": "price",
    "idOld": "price",
    "label": "Precio total al contado",
    "type": "attribute",
    "help": "Debe de llenar el precio total al contado y sin decimales al que se ofece al auto",
    "alert": "Debe de llenar el precio total al contado y sin decimales al que se ofece al auto",
    "placeholder": "Escriba el precio de venta",
    "inputType": "text",
    "valueType": "number",
    "required": true,
    "groupId": "basic",
    "allowEdit": true,
    "order": 0,
    "value": -1
  },
  "currency": {
    "id": "currency",
    "idOld": "currencies",
    "label": "Moneda",
    "type": "attribute",
    "help": "",
    "alert": "",
    "placeholder": "",
    "inputType": "enum",
    "valueType": "text",
    "required": true,
    "groupId": "basic",
    "allowEdit": true,
    "order": 1,
    "valueId": "CURRENCIE-MXN",
    "values": [{
        "id": "CURRENCIE-MXN",
        "name": "MXN"
      },
      {
        "id": "CURRENCIE-USD",
        "name": "USD"
      }
    ]
  },
  "priceType": {
    "id": "priceType",
    "idOld": "",
    "label": "Tipo de precio",
    "type": "attribute",
    "help": "Seleccione el tipo de precio listado",
    "alert": "Seleccione el tipo de precio listado",
    "placeholder": "",
    "inputType": "enum",
    "valueType": "text",
    "required": true,
    "groupId": "basic",
    "allowEdit": true,
    "order": 2,
    "valueId": "PRICETYPE-NEGOTIABLE",
    "values": [{
        "id": "PRICETYPE-NEGOTIABLE",
        "name": "Negociable"
      },
      {
        "id": "PRICETYPE-FIXED",
        "name": "Fijo"
      },
      {
        "id": "PRICETYPE-SAVINGS",
        "name": "Plan de ahorro"
      },
      {
        "id": "PRICETYPE-FINANCED",
        "name": "Financiado"
      },
      {
        "id": "PRICETYPE-MONTHLY_RENT",
        "name": "Renta Mensual"
      }
    ]
  },
  "odometer": {
    "id": "odometer",
    "idOld": "kilometers",
    "label": "Odómetro",
    "type": "attribute",
    "help": "",
    "alert": "",
    "placeholder": "",
    "inputType": "text",
    "valueType": "number",
    "required": true,
    "groupId": "basic",
    "allowEdit": true,
    "order": 0,
    "value": "-1"
  },
  "odometerUnits": {
    "id": "odometerUnits",
    "idOld": "",
    "label": "Unidades",
    "type": "attribute",
    "help": "",
    "alert": "",
    "placeholder": "",
    "inputType": "enum",
    "valueType": "text",
    "required": true,
    "groupId": "basic",
    "allowEdit": true,
    "order": 1,
    "valueId": "ODOMETER_UNITS-KM",
    "values": [{
        "id": "ODOMETER_UNITS-KM",
        "name": "Km"
      },
      {
        "id": "ODOMETER_UNITS-MI",
        "name": "Mi"
      },
      {
        "id": "ODOMETER_UNITS-HR",
        "name": "Hrs"
      }
    ]
  },
  "odometerStatus": {
    "id": "odometerStatus",
    "idOld": "",
    "label": "Estatus",
    "type": "attribute",
    "help": "",
    "alert": "",
    "placeholder": "",
    "inputType": "enum",
    "valueType": "text",
    "required": false,
    "groupId": "basic",
    "allowEdit": true,
    "order": 2,
    "valueId": "ODOMETER_STATUS-ORIGINAL",
    "values": [{
        "id": "ODOMETER_STATUS-UNKNOWN",
        "name": "Desconocido"
      },
      {
        "id": "ODOMETER_STATUS-ROLLEDOVER",
        "name": "Dio la vuelta"
      },
      {
        "id": "ODOMETER_STATUS-REPLACED",
        "name": "Reemplazado"
      },
      {
        "id": "ODOMETER_STATUS-ORIGINAL",
        "name": "Original"
      }
    ]
  },
  "priceGroup": {
    "id": "priceGroup",
    "idOld": "",
    "label": "Precio",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "attribute",
    "inputType": "text",
    "valueType": "group",
    "required": true,
    "groupId": "basic",
    "allowEdit": true,
    "order": 0
  },
  "odometerGroup": {
    "id": "odometerGroup",
    "idOld": "",
    "label": "Odómetro",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "attribute",
    "inputType": "text",
    "valueType": "group",
    "required": true,
    "groupId": "basic",
    "allowEdit": true,
    "order": 1
  },
  "vin": {
    "id": "vin",
    "idOld": "VIN",
    "label": "Número de identificación vehicular",
    "type": "attribute",
    "help": "",
    "alert": "",
    "placeholder": "",
    "inputType": "text",
    "valueType": "text",
    "required": false,
    "groupId": "basic",
    "allowEdit": false,
    "order": 2,
    "value": ""
  },
  "condition": {
    "id": "condition",
    "idOld": "condition",
    "label": "Condición",
    "type": "attribute",
    "help": "",
    "alert": "",
    "placeholder": "",
    "inputType": "enum",
    "valueType": "text",
    "required": true,
    "groupId": "basic",
    "allowEdit": false,
    "order": 3,
    "valueId": "CONDITION-USED",
    "values": [{
        "id": "CONDITION-USED",
        "name": "Usado"
      },
      {
        "id": "CONDITION-NEW",
        "name": "Nuevo"
      },
      {
        "id": "CONDITION-IMPORTED",
        "name": "Importado"
      },
      {
        "id": "CONDITION-HILLY",
        "name": "Siniestrado"
      },
      {
        "id": "CONDITION-UNSPECIFIED",
        "name": "Sin Especificar"
      }
    ]
  },
  "transmision":{
    "id" : "transmision",
    "idOld" : "transmition",
    "label" : "Transmisión",
    "type" : "attribute",
    "help" : "",
    "alert" : "",
    "placeholder" : "",
    "inputType" : "enum",
    "valueType" : "text",
    "required" : true,
    "groupId" : "basic",
    "allowEdit" : false,
    "order" : 4,
    "valueId" : "",
    "values" : [ 
        {
            "id" : "TRANSMISION-SEQUENTIAL",
            "name" : "Secuencial"
        }, 
        {
            "id" : "TRANSMISION-MANUAL",
            "name" : "Manual"
        }, 
        {
            "id" : "TRANSMISION-AUTOMATICA",
            "name" : "Automática"
        }, 
        {
            "id" : "TRANSMISION-SEMI_AUTOMATICA",
            "name" : "Semi-automática"
        }, 
        {
            "id" : "CONDITION-UNSPECIFIED",
            "name" : "Sin Especificar"
        }
    ]
},
"direction":{
  "id" : "direction",
  "idOld" : "direction",
  "label" : "Dirección",
  "type" : "attribute",
  "help" : "",
  "alert" : "",
  "placeholder" : "",
  "inputType" : "enum",
  "valueType" : "text",
  "required" : true,
  "groupId" : "basic",
  "allowEdit" : false,
  "order" : 5,
  "valueId" : "",
  "values" : [ 
      {
          "id" : "DIRECTION-ASSISTED",
          "name" : "Asistida"
      }, 
      {
          "id" : "DIRECTION-HIDRAULICS",
          "name" : "Hidráulica"
      }, 
      {
          "id" : "DIRECTION-MECHANICS",
          "name" : "Mecánica"
      }
  ]
},
  "loadingCapacity": {
    "id": "loadingCapacity",
    "idOld": "",
    "label": "Capacidad de carga (Kg)",
    "type": "attribute",
    "help": "",
    "alert": "",
    "placeholder": "",
    "inputType": "text",
    "valueType": "number",
    "required": false,
    "groupId": "basic",
    "allowEdit": false,
    "order": 6,
    "value": ""
  },
  "ballastCapacity": {
    "id": "ballastCapacity",
    "idOld": "",
    "label": "Capacidad de lastre (Kg)",
    "type": "attribute",
    "help": "",
    "alert": "",
    "placeholder": "",
    "inputType": "text",
    "valueType": "number",
    "required": false,
    "groupId": "basic",
    "allowEdit": false,
    "order": 7,
    "value": ""
  },
  "colorExt": {
    "id": "colorExt",
    "idOld": "colorExt",
    "label": "Color exterior",
    "type": "attribute",
    "help": "",
    "alert": "",
    "placeholder": "",
    "inputType": "enum",
    "valueType": "text",
    "required": true,
    "groupId": "basic",
    "allowEdit": true,
    "order": 8,
    "valueId": "",
    "values": [{
        "id": "COLOREXT-AMARILLO",
        "name": "Amarillo"
      },
      {
        "id": "COLOREXT-AZUL",
        "name": "Azul"
      },
      {
        "id": "COLOREXT-BEIGE",
        "name": "Beige"
      },
      {
        "id": "COLOREXT-BLANCO",
        "name": "Blanco"
      },
      {
        "id": "COLOREXT-CAFE",
        "name": "Café"
      },
      {
        "id": "COLOREXT-CREMA",
        "name": "Crema"
      },
      {
        "id": "COLOREXT-DORADO",
        "name": "Dorado"
      },
      {
        "id": "COLOREXT-GRIS",
        "name": "Gris"
      },
      {
        "id": "COLOREXT-MARRON",
        "name": "Marrón"
      },
      {
        "id": "COLOREXT-MORADO",
        "name": "Morado"
      },
      {
        "id": "COLOREXT-ANARANJADO",
        "name": "Naranja"
      },
      {
        "id": "COLOREXT-NEGRO",
        "name": "Negro"
      },
      {
        "id": "COLOREXT-PLATEADO",
        "name": "Plata"
      },
      {
        "id": "COLOREXT-ROJO",
        "name": "Rojo"
      },
      {
        "id": "COLOREXT-VERDE",
        "name": "Verde"
      },
      {
        "id": "COLOREXT-VINO_TINTO",
        "name": "Vino"
      },
      {
        "id": "COLOREXT-OTRO",
        "name": "Otro"
      }
    ]
  },
  "colorInt": {
    "id": "colorInt",
    "idOld": "colorInt",
    "label": "Color interior2",
    "type": "attribute",
    "help": "",
    "alert": "",
    "placeholder": "",
    "inputType": "enum",
    "valueType": "text",
    "required": false,
    "groupId": "basic",
    "allowEdit": true,
    "order": 9,
    "valueId": "",
    "values": [{
        "id": "COLORINT-BEIGE",
        "name": "Beige"
      },
      {
        "id": "COLORINT-CAFE",
        "name": "Café"
      },
      {
        "id": "COLORINT-GRIS",
        "name": "Gris"
      },
      {
        "id": "COLORINT-NEGRO",
        "name": "Negro"
      },
      {
        "id": "COLORINT-OTRO",
        "name": "Otro"
      }
    ]
  },
  "vesture": {
    "id": "vesture",
    "idOld": "vesture",
    "label": "Vestidura",
    "type": "attribute",
    "help": "",
    "alert": "",
    "placeholder": "",
    "inputType": "enum",
    "valueType": "text",
    "required": true,
    "groupId": "basic",
    "allowEdit": true,
    "order": 10,
    "valueId": "",
    "values": [{
        "id": "VESTURE-TELA",
        "name": "Tela"
      },
      {
        "id": "VESTURE-PIEL",
        "name": "Piel (Cuero)"
      },
      {
        "id": "VESTURE-OTRO",
        "name": "Otro"
      }
    ]
  },
  "fuel": {
    "id": "fuel",
    "idOld": "fuel",
    "label": "Combustible",
    "type": "attribute",
    "help": "",
    "alert": "",
    "placeholder": "",
    "inputType": "enum",
    "valueType": "text",
    "required": true,
    "groupId": "basic",
    "allowEdit": true,
    "order": 11,
    "valueId": "FUEL-DIESEL",
    "values": [{
        "id": "FUEL-DIESEL",
        "name": "Diesel"
      },
      {
        "id": "FUEL-GASOLINA",
        "name": "Gasolina"
      },
      {
        "id": "FUEL-GASOLINA_PRE",
        "name": "Gasolina premium"
      },
      {
        "id": "FUEL-GAS_LP",
        "name": "Gas Lp"
      },
      {
        "id": "FUEL-ELECTRICO",
        "name": "Eléctrico"
      },
      {
        "id": "FUEL-HIBRIDO",
        "name": "Híbrido"
      },
      {
        "id": "FUEL-OTRO",
        "name": "Otro"
      }
    ]
  },
  "finantial": {
    "id": "finantial",
    "idOld": "",
    "label": "Financiamiento",
    "type": "attribute",
    "help": "",
    "alert": "",
    "placeholder": "",
    "inputType": "enum",
    "valueType": "text",
    "required": false,
    "groupId": "basic",
    "allowEdit": true,
    "order": 12,
    "valueId": "",
    "values": [{
      "id": "FINANTIAL--1",
      "name": "Agregar Financiamiento"
    }]
  },
  "waranty": {
    "id": "waranty",
    "idOld": "",
    "label": "Garantía",
    "type": "attribute",
    "help": "",
    "alert": "",
    "placeholder": "",
    "inputType": "enum",
    "valueType": "text",
    "required": false,
    "groupId": "basic",
    "allowEdit": true,
    "order": 13,
    "valueId": "",
    "values": [{
      "id": "WARANTY--1",
      "name": "Agregar Garantía"
    }]
  },
  "descriptionTxt": {
    "id": "descriptionTxt",
    "idOld": "description",
    "label": "Descripción en texto plano",
    "type": "attribute",
    "help": "",
    "alert": "",
    "placeholder": "",
    "inputType": "objectId",
    "valueType": "text",
    "required": true,
    "groupId": "basic",
    "allowEdit": true,
    "order": 14,
    "value": "-1"
  },
  "descriptionHtml": {
    "id": "descriptionHtml",
    "idOld": "description_html",
    "label": "Descripción en HTML",
    "type": "attribute",
    "help": "",
    "alert": "",
    "placeholder": "",
    "inputType": "objectId",
    "valueType": "text",
    "required": false,
    "groupId": "basic",
    "allowEdit": true,
    "order": 15,
    "value": "-1"
  },
  "suspension": {
    "id": "suspension",
    "idOld": "",
    "label": "Suspensión",
    "type": "attribute",
    "help": "",
    "alert": "",
    "placeholder": "",
    "inputType": "enum",
    "valueType": "text",
    "required": true,
    "groupId": "basic",
    "allowEdit": true,
    "order": 16,
    "valueId": "",
    "values": [{
        "id": "ENGINE-AIR",
        "name": "Air"
      },
      {
        "id": "ENGINE-BEAM",
        "name": "Beam"
      },
      {
        "id": "ENGINE-SPRING",
        "name": "Spring"
      },
      {
        "id": "ENGINE-OTRA",
        "name": "Otra"
      }
    ]
  },
  "badge": {
    "id": "badge",
    "oldId": "badge",
    "label": "Distintivo",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "attribute",
    "inputType": "enum",
    "valueType": "text",
    "required": false,
    "groupId": "basic",
    "allowEdit": true,
    "order": 18,
    "valueId": "",
    "values": [
      {
        "id": "BADGE-BONO",
        "name": "Bono",
        "color": '#ffffff',
        "background-color" : '#0099cc'
      },
      {
        "id": "BADGE-DEMO",
        "name": "Demo",
        "color": '#ffffff',
        "background-color" : '#cc9933'
      },
      {
        "id": "BADGE-CERTIFICADO",
        "name": "Certificado",
        "color": '#ffffff',
        "background-color" : '#cc0033'
      },
      {
        "id": "BADGE-IVA",
        "name": "IVA",
        "color": '#ffffff',
        "background-color" : '#339966'
      },
      {
        "id": "BADGE-NOVEDAD",
        "name": "Novedad",
        "color": '#ffffff',
        "background-color" : '#333399'
      },
      {
        "id": "PERSONAL",
        "name": "Promoción",
        "color": '#ffffff',
        "background-color" : '#fa9537'
      }
    ]
  },
  "speeds": {
    "id": "speeds",
    "idOld": "",
    "label": "Velocidades",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "attribute",
    "inputType": "enum",
    "valueType": "text",
    "required": false,
    "groupId": "advanced",
    "allowEdit": true,
    "order": 0,
    "valueId": "",
    "values": [{
        "id": "SPEEDS-8LL",
        "name": "8LL"
      },
      {
        "id": "SPEEDS-AUTO-10SPD",
        "name": "Auto-10Spd"
      },
      {
        "id": "SPEEDS-AUTO-12 SPEED",
        "name": "Auto-12 speed"
      },
      {
        "id": "SPEEDS-AUTO-2SPD",
        "name": "Auto-2Spd"
      },
      {
        "id": "SPEEDS-AUTO-3SPD",
        "name": "Auto-3Spd"
      },
      {
        "id": "SPEEDS-AUTO-4SPD",
        "name": "Auto-4Spd"
      },
      {
        "id": "SPEEDS-AUTO-5SPD",
        "name": "Auto-5Spd"
      },
      {
        "id": "SPEEDS-AUTO-6SPD",
        "name": "Auto-6Spd"
      },
      {
        "id": "SPEEDS-AUTO-7SPD",
        "name": "Auto-7spd"
      },
      {
        "id": "SPEEDS-AUTO-8SPD",
        "name": "Auto-8spd"
      },
      {
        "id": "SPEEDS-AUTO-9SPD",
        "name": "Auto-9spd"
      },
      {
        "id": "SPEEDS-AUTOMATIC",
        "name": "Automatic"
      },
      {
        "id": "SPEEDS-MAN-10SPD",
        "name": "Man-10Spd"
      },
      {
        "id": "SPEEDS-MAN-12SPD",
        "name": "Man-12Spd"
      },
      {
        "id": "SPEEDS-MAN-13SPD",
        "name": "Man-13Spd"
      },
      {
        "id": "SPEEDS-MAN-15SPD",
        "name": "Man-15Spd"
      },
      {
        "id": "SPEEDS-MAN-18SPD",
        "name": "Man-18Spd"
      },
      {
        "id": "SPEEDS-MAN-3SPD",
        "name": "Man-3Spd"
      },
      {
        "id": "SPEEDS-MAN-4SPD",
        "name": "Man-4Spd"
      },
      {
        "id": "SPEEDS-MAN-5SPD",
        "name": "Man-5Spd"
      },
      {
        "id": "SPEEDS-MAN-6SPD",
        "name": "Man-6Spd"
      },
      {
        "id": "SPEEDS-MAN-7SPD",
        "name": "Man-7Spd"
      },
      {
        "id": "SPEEDS-MAN-8SPD",
        "name": "Man-8Spd"
      },
      {
        "id": "SPEEDS-MAN-9SPD",
        "name": "Man-9Spd"
      },
      {
        "id": "SPPEDS-OTRO",
        "name": "Otro"
      }
    ]
  },
  "transmissionBrand": {
    "id": "transmissionBrand",
    "idOld": "",
    "label": "Marca de la transmisión",
    "type": "attribute",
    "help": "",
    "alert": "",
    "placeholder": "",
    "valueType": "text",
    "inputType": "text",
    "required": true,
    "groupId": "advanced",
    "allowEdit": true,
    "order": 1,
    "value": "-1"
  },
  "power": {
    "id": "power",
    "idOld": "",
    "label": "Potencia máxima en HP",
    "help": "Colocar la potencia del motor cuando este fue vendido como nuevo",
    "alert": "Solo se aceptan números",
    "placeholder": "Por ej. 500",
    "type": "attribute",
    "inputType": "text",
    "valueType": "number",
    "required": true,
    "groupId": "advanced",
    "allowEdit": true,
    "order": 2,
    "value": -1
  },
  "torque": {
    "id": "torque",
    "idOld": "",
    "label": "Torque máximo libras/pie",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "attribute",
    "inputType": "text",
    "valueType": "number",
    "required": false,
    "groupId": "advanced",
    "allowEdit": true,
    "order": 3,
    "value": -1
  },
  "engineManufacturer": {
    "id": "engineManufacturer",
    "idOld": "",
    "label": "Fabricante del motor",
    "type": "attribute",
    "help": "",
    "alert": "",
    "placeholder": "",
    "inputType": "enum",
    "valueType": "text",
    "required": false,
    "groupId": "advanced",
    "allowEdit": true,
    "order": 4,
    "valueId": "",
    "values": [{
        "id": "ENGINE-CATERPILLAR",
        "name": "Caterpillar"
      },
      {
        "id": "ENGINE-CHEVROLET",
        "name": "Chevrolet"
      },
      {
        "id": "ENGINE-CHRYSLER",
        "name": "Chrysler"
      },
      {
        "id": "ENGINE-CONTINENTAL",
        "name": "Continental"
      },
      {
        "id": "ENGINE-CUMMINS",
        "name": "Cummins"
      },
      {
        "id": "ENGINE-DETROIT",
        "name": "Detroit"
      },
      {
        "id": "ENGINE-DMC",
        "name": "DMC"
      },
      {
        "id": "ENGINE-DODGE",
        "name": "Dodge"
      },
      {
        "id": "ENGINE-DURAMAX",
        "name": "Duramax"
      },
      {
        "id": "ENGINE-EATON",
        "name": "Eaton"
      },
      {
        "id": "ENGINE-FORD",
        "name": "Ford"
      },
      {
        "id": "ENGINE-GMC",
        "name": "GMC"
      },
      {
        "id": "ENGINE-HERCULES",
        "name": "Hercules"
      },
      {
        "id": "ENGINE-HINO",
        "name": "Hino"
      },
      {
        "id": "ENGINE-INTERNATIONAL",
        "name": "International"
      },
      {
        "id": "ENGINE-ISUZU",
        "name": "Isuzu"
      },
      {
        "id": "ENGINE-JOHN DEERE",
        "name": "John Deere"
      },
      {
        "id": "ENGINE-MACK",
        "name": "Mack"
      },
      {
        "id": "ENGINE-MERCEDES BENZ",
        "name": "MERCEDES BENZ"
      },
      {
        "id": "ENGINE-MITSUBISHI",
        "name": "Mitsubishi"
      },
      {
        "id": "ENGINE-NAVISTAR",
        "name": "Navistar"
      },
      {
        "id": "ENGINE-NISSAN",
        "name": "Nissan"
      },
      {
        "id": "ENGINE-OTHER",
        "name": "Other"
      },
      {
        "id": "ENGINE-PACCAR",
        "name": "Paccar"
      },
      {
        "id": "ENGINE-POWERSTROKE",
        "name": "Powerstroke"
      },
      {
        "id": "ENGINE-RENAULT",
        "name": "Renault"
      },
      {
        "id": "ENGINE-VOLVO",
        "name": "Volvo"
      },
      {
        "id": "ENGINE-WHITE",
        "name": "White"
      },
      {
        "id": "ENGINE-OTRO",
        "name": "Otro"
      }
    ]
  },
  "engineModel": {
    "id": "engineModel",
    "idOld": "",
    "label": "Modelo del motor",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "attribute",
    "inputType": "text",
    "valueType": "text",
    "required": true,
    "groupId": "advanced",
    "allowEdit": true,
    "order": 5,
    "value": -1
  },
  "displacement": {
    "id": "displacement",
    "idOld": "",
    "label": "Cilindrada",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "attribute",
    "inputType": "text",
    "valueType": "number",
    "required": false,
    "groupId": "advanced",
    "allowEdit": true,
    "order": 6,
    "value": -1
  },
  "tankCapacity": {
    "id": "tankCapacity",
    "idOld": "",
    "label": "Capacidad del tanque (l)",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "attribute",
    "inputType": "text",
    "valueType": "number",
    "required": false,
    "groupId": "advanced",
    "allowEdit": true,
    "order": 7,
    "value": -1
  },
  "frontAxles": {
    "id": "frontAxles",
    "idOld": "",
    "label": "Ejes frontales (lb)",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "attribute",
    "inputType": "text",
    "valueType": "number",
    "required": true,
    "groupId": "advanced",
    "allowEdit": true,
    "order": 8,
    "value": -1
  },
  "rearAxles": {
    "id": "rearAxles",
    "idOld": "",
    "label": "Ejes traseros (lb)",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "attribute",
    "inputType": "text",
    "valueType": "number",
    "required": true,
    "groupId": "advanced",
    "allowEdit": true,
    "order": 9,
    "value": -1
  },
  "step": {
    "id": "step",
    "idOld": "",
    "label": "Paso",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "attribute",
    "inputType": "text",
    "valueType": "number",
    "required": true,
    "groupId": "advanced",
    "allowEdit": true,
    "order": 10,
    "value": -1
  },
  "diameterOfWheels": {
    "id": "diameterOfWheels",
    "idOld": "",
    "label": "Diámetro de rines (pulgadas)",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "attribute",
    "inputType": "text",
    "valueType": "number",
    "required": false,
    "groupId": "advanced",
    "allowEdit": true,
    "order": 11,
    "value": -1
  },
  "stock": {
    "id": "stock",
    "idOld": "stock_number",
    "label": "Numero de inventario",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "attribute",
    "inputType": "text",
    "valueType": "text",
    "required": true,
    "groupId": "business",
    "allowEdit": false,
    "order": 0,
    "value": ""
  },
  "purchasePrice": {
    "id": "purchasePrice",
    "idOld": "",
    "label": "Precio de compra",
    "help": "Precio al que se compró el auto",
    "alert": "",
    "placeholder": "Por ej. $268000",
    "type": "attribute",
    "inputType": "text",
    "valueType": "number",
    "required": false,
    "groupId": "business",
    "allowEdit": false,
    "order": 1,
    "value": ""
  },
  "purchaseDate": {
    "id": "purchaseDate",
    "idOld": "",
    "label": "Fecha de compra",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "attribute",
    "inputType": "text",
    "valueType": "date",
    "required": false,
    "groupId": "business",
    "allowEdit": false,
    "order": 2,
    "value": ""
  },
  "brand": {
    "id": "brand",
    "idOld": "mark",
    "label": "Marca",
    "type": "category",
    "help": "",
    "alert": "",
    "placeholder": "",
    "inputType": "enum",
    "valueType": "text",
    "required": true,
    "groupId": "catalog",
    "allowEdit": false,
    "order": 0,
    "valueId": "",
    "value": ""
  },
  "model": {
    "id": "model",
    "idOld": "model",
    "label": "Modelo",
    "type": "category",
    "help": "",
    "alert": "",
    "placeholder": "",
    "inputType": "enum",
    "valueType": "text",
    "required": true,
    "groupId": "catalog",
    "allowEdit": false,
    "order": 1,
    "valueId": "",
    "value": ""
  },
  "year": {
    "id": "year",
    "idOld": "year",
    "label": "Año",
    "type": "category",
    "help": "",
    "alert": "",
    "placeholder": "",
    "inputType": "enum",
    "valueType": "text",
    "required": true,
    "groupId": "catalog",
    "allowEdit": false,
    "order": 2,
    "valueId": "",
    "value": ""
  },
  "trim": {
    "id": "trim",
    "idOld": "version",
    "label": "Versión",
    "type": "category",
    "help": "",
    "alert": "",
    "placeholder": "",
    "inputType": "enum",
    "valueType": "text",
    "required": false,
    "groupId": "catalog",
    "allowEdit": false,
    "order": 3,
    "valueId": "",
    "value": ""
  },
  "5thWheelHitch": {
    "id": "5thWheelHitch",
    "idOld": "",
    "label": "5ª rueda de enganche",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "equipment",
    "inputType": "boolean",
    "valueType": "boolean",
    "required": false,
    "groupId": "exterior",
    "allowEdit": true,
    "order": 0,
    "value": false
  },
  "fairings": {
    "id": "fairings",
    "idOld": "",
    "label": "Carenados",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "equipment",
    "inputType": "boolean",
    "valueType": "boolean",
    "required": false,
    "groupId": "exterior",
    "allowEdit": true,
    "order": 1,
    "value": false
  },
  "stepBumper": {
    "id": "stepBumper",
    "idOld": "",
    "label": "Escalón en parachoques",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "equipment",
    "inputType": "boolean",
    "valueType": "boolean",
    "required": false,
    "groupId": "exterior",
    "allowEdit": true,
    "order": 2,
    "value": false
  },
  "powerMirrors": {
    "id": "powerMirrors",
    "idOld": "power_mirrors",
    "label": "Espejos laterales",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "equipment",
    "inputType": "boolean",
    "valueType": "boolean",
    "required": false,
    "groupId": "exterior",
    "allowEdit": true,
    "order": 3,
    "value": false
  },
  "fogLamps": {
    "id": "fogLamps",
    "idOld": "fog_lamps",
    "label": "Faros de niebla",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "equipment",
    "inputType": "boolean",
    "valueType": "boolean",
    "required": false,
    "groupId": "exterior",
    "allowEdit": true,
    "order": 4,
    "value": false
  },
  "alliedTeam": {
    "id": "alliedTeam",
    "idOld": "",
    "label": "Equipo aliado",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "equhttps://www.npmjs.com/package/vin-libipment",
    "inputType": "boolean",
    "valueType": "boolean",
    "required": false,
    "groupId": "exterior",
    "allowEdit": true,
    "order": 5,
    "value": false
  },
  "bodySideMoldings": {
    "id": "bodySideMoldings",
    "idOld": "",
    "label": "Molduras laterales",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "equipment",
    "inputType": "boolean",
    "valueType": "boolean",
    "required": false,
    "groupId": "exterior",
    "allowEdit": true,
    "order": 6,
    "value": false
  },
  "liftGate": {
    "id": "liftGate",
    "idOld": "",
    "label": "Puerta trasera",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "equipment",
    "inputType": "boolean",
    "valueType": "boolean",
    "required": false,
    "groupId": "exterior",
    "allowEdit": true,
    "order": 7,
    "value": false
  },
  "sunroof": {
    "id": "sunroof",
    "idOld": "sunroof",
    "label": "Quemacocos",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "equipment",
    "inputType": "boolean",
    "valueType": "boolean",
    "required": false,
    "groupId": "exterior",
    "allowEdit": true,
    "order": 8,
    "value": false
  },
  "alloyWheels": {
    "id": "alloyWheels",
    "idOld": "alloy_wheels",
    "label": "Rines de aluminio",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "equipment",
    "inputType": "boolean",
    "valueType": "boolean",
    "required": false,
    "groupId": "exterior",
    "allowEdit": true,
    "order": 9,
    "value": false
  },
  "chromeWheels": {
    "id": "chromeWheels",
    "idOld": "",
    "label": "Rines de cromados",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "equipment",
    "inputType": "boolean",
    "valueType": "boolean",
    "required": false,
    "groupId": "exterior",
    "allowEdit": true,
    "order": 10,
    "value": false
  },
  "aluminumWheels": {
    "id": "aluminumWheels",
    "idOld": "",
    "label": "Ruedas de aluminio",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "equipment",
    "inputType": "boolean",
    "valueType": "boolean",
    "required": false,
    "groupId": "exterior",
    "allowEdit": true,
    "order": 11,
    "value": false
  },
  "airCondition": {
    "id": "airCondition",
    "idOld": "",
    "label": "Aire acondicionado",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "equipment",
    "inputType": "boolean",
    "valueType": "boolean",
    "required": false,
    "groupId": "interior",
    "allowEdit": true,
    "order": 0,
    "value": false
  },
  "conditioningAirSeat": {
    "id": "conditioningAirSeat",
    "idOld": "",
    "label": "Aire acondicionado en el asiento",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "equipment",
    "inputType": "boolean",
    "valueType": "boolean",
    "required": false,
    "groupId": "interior",
    "allowEdit": true,
    "order": 1,
    "value": false
  },
  "powerSeats": {
    "id": "powerSeats",
    "idOld": "power_seats",
    "label": "Asiento conductor",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "equipment",
    "inputType": "boolean",
    "valueType": "boolean",
    "required": false,
    "groupId": "interior",
    "allowEdit": true,
    "order": 2,
    "value": false
  },
  "bluetooth": {
    "id": "bluetooth",
    "idOld": "bluetooth",
    "label": "Bluetooth",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "equipment",
    "inputType": "boolean",
    "valueType": "boolean",
    "required": false,
    "groupId": "interior",
    "allowEdit": true,
    "order": 3,
    "value": false
  },
  "parkingCamera": {
    "id": "parkingCamera",
    "idOld": "parking_camera",
    "label": "Cámara de estacionamiento",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "equipment",
    "inputType": "boolean",
    "valueType": "boolean",
    "required": false,
    "groupId": "interior",
    "allowEdit": true,
    "order": 4,
    "value": false
  },
  "cassette": {
    "id": "cassette",
    "idOld": "",
    "label": "Casete",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "equipment",
    "inputType": "boolean",
    "valueType": "boolean",
    "required": false,
    "groupId": "interior",
    "allowEdit": true,
    "order": 5,
    "value": false
  },
  "cruiseControl": {
    "id": "cruiseControl",
    "idOld": "cruise_control",
    "label": "Control automático de velocidad",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "equipment",
    "inputType": "boolean",
    "valueType": "boolean",
    "required": false,
    "groupId": "interior",
    "allowEdit": true,
    "order": 6,
    "value": false
  },
  "powerWindows": {
    "id": "powerWindows",
    "idOld": "power_windows",
    "label": "Cristales delanteros",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "equipment",
    "inputType": "boolean",
    "valueType": "boolean",
    "required": false,
    "groupId": "interior",
    "allowEdit": true,
    "order": 7,
    "value": false
  },
  "centerArmRest": {
    "id": "centerArmRest",
    "idOld": "",
    "label": "Descanza brazo central",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "equipment",
    "inputType": "boolean",
    "valueType": "boolean",
    "required": false,
    "groupId": "interior",
    "allowEdit": true,
    "order": 8,
    "value": false
  },
  "rearDefroster": {
    "id": "rearDefroster",
    "idOld": "rear_defroster",
    "label": "Desempañador trasero",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "equipment",
    "inputType": "boolean",
    "valueType": "boolean",
    "required": false,
    "groupId": "interior",
    "allowEdit": true,
    "order": 9,
    "value": false
  },
  "powerSteering": {
    "id": "powerSteering",
    "idOld": "",
    "label": "Dirección asistida",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "equipment",
    "inputType": "boolean",
    "valueType": "boolean",
    "required": false,
    "groupId": "interior",
    "allowEdit": true,
    "order": 10,
    "value": false
  },
  "tripOdometer": {
    "id": "tripOdometer",
    "idOld": "",
    "label": "Odómetro de viaje",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "equipment",
    "inputType": "boolean",
    "valueType": "boolean",
    "required": false,
    "groupId": "interior",
    "allowEdit": true,
    "order": 11,
    "value": false
  },
  "radioAmFm": {
    "id": "radioAmFm",
    "idOld": "",
    "label": "Radio AM / FM",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "equipment",
    "inputType": "boolean",
    "valueType": "boolean",
    "required": false,
    "groupId": "interior",
    "allowEdit": true,
    "order": 12,
    "value": false
  },
  "cb": {
    "id": "cb",
    "idOld": "",
    "label": "Radio banda civil",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "equipment",
    "inputType": "boolean",
    "valueType": "boolean",
    "required": false,
    "groupId": "interior",
    "allowEdit": true,
    "order": 13,
    "value": false
  },
  "cdPlayer": {
    "id": "cdPlayer",
    "idOld": "cd_player",
    "label": "Reproductor CD/MP3",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "equipment",
    "inputType": "boolean",
    "valueType": "boolean",
    "required": false,
    "groupId": "interior",
    "allowEdit": true,
    "order": 14,
    "value": false
  },
  "powerDoorLocks": {
    "id": "powerDoorLocks",
    "idOld": "power_door_locks",
    "label": "Seguros",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "equipment",
    "inputType": "boolean",
    "valueType": "boolean",
    "required": false,
    "groupId": "interior",
    "allowEdit": true,
    "order": 15,
    "value": false
  },
  "phone": {
    "id": "phone",
    "idOld": "",
    "label": "Teléfono",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "equipment",
    "inputType": "boolean",
    "valueType": "boolean",
    "required": false,
    "groupId": "interior",
    "allowEdit": true,
    "order": 16,
    "value": false
  },
  "slidingRearWindow": {
    "id": "slidingRearWindow",
    "idOld": "",
    "label": "Ventana trasera deslizante",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "equipment",
    "inputType": "boolean",
    "valueType": "boolean",
    "required": false,
    "groupId": "interior",
    "allowEdit": true,
    "order": 17,
    "value": false
  },
  "tintedWindows": {
    "id": "tintedWindows",
    "idOld": "tinted_windows",
    "label": "Vidrios entintados",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "equipment",
    "inputType": "boolean",
    "valueType": "boolean",
    "required": false,
    "groupId": "interior",
    "allowEdit": true,
    "order": 18,
    "value": false
  },
  "wheelPositions": {
    "id": "wheelPositions",
    "idOld": "wheel_positions",
    "label": "Volante de posiciones",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "equipment",
    "inputType": "boolean",
    "valueType": "boolean",
    "required": false,
    "groupId": "interior",
    "allowEdit": true,
    "order": 19,
    "value": false
  },
  "dialAirBag": {
    "id": "dialAirBag",
    "idOld": "",
    "label": "Bolsa de seguridad dual",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "equipment",
    "inputType": "boolean",
    "valueType": "boolean",
    "required": false,
    "groupId": "security",
    "allowEdit": true,
    "order": 0,
    "value": false
  },
  "jakeBrake": {
    "id": "jakeBrake",
    "idOld": "",
    "label": "Freno de motor",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "equipment",
    "inputType": "boolean",
    "valueType": "boolean",
    "required": false,
    "groupId": "security",
    "allowEdit": true,
    "order": 1,
    "value": false
  },
  "backupSensorSystem": {
    "id": "backupSensorSystem",
    "idOld": "",
    "label": "Sistema de sensores de respaldo",
    "help": "",
    "alert": "",
    "placeholder": "",
    "type": "equipment",
    "inputType": "boolean",
    "valueType": "boolean",
    "required": false,
    "groupId": "security",
    "allowEdit": true,
    "order": 2,
    "value": false
  }
}