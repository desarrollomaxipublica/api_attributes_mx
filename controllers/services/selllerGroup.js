module.exports = {
    "id": "sellerId",
    "oldId": '',
    "label": "Nombre de la Agencia",
    "type": "attribute",
    "help": "",
    "alert": "",
    "placeholder": "",
    "inputType": "enum",
    "valueType": "text",
    "required": true,
    "groupId": "basic",
    "allowEdit": true,
    "order": 19,
    "valueId": "",
    "values": []
}