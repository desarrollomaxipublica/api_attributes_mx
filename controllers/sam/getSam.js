'use strict'
const dbAttributes = require('../../models/attributesMxp')
const mysql = require('mysql'),
    request = require('request'),
    base64url = require('base64url')
const config = {
    host: 'panel.soloautos.mx',
    user: 'maxipublica_usr',
    port: 3306,
    password: 'tM_mSMNv4_lmlv'
}
const nameGroups = {
    basic: 'Publicación',
    advanced: "Avanzado",
    business: 'Inteligencia de negocios',
    audio: 'audio',
    confort: 'Confort',
    documents: 'Documentos',
    electric: 'Eléctrico',
    extra: 'Extras',
    gadgets: 'Gadgets',
    security: 'Seguridad',
    interior: 'Exterior',
    exterior: 'Exterior',
    catalog: 'Catálogo'
}

exports.validateToken = (req, res, next) => {
    if (req.query.token) {
        let url = 'http://d17.maxipublica.com/oauth/valid/' + req.query.token
        request.get({
            url: url
        }, (err, response, body) => {
            if (!err) {
                if (response.statusCode == 200) {
                    body = JSON.parse(body)
                    req.token = {}
                    req.body.userId = base64url.decode(body.userId)
                    req.body.userId = base64url.decode(body.userId)
                    req.token.sellerId = base64url.decode(body.sellerId)
                    req.token.rol = base64url.decode(body.rol)
                    req.token.type = base64url.decode(body.type)
                    next()
                }
            }
        })
    }
}
//consultar los attributos para la version del idJATO
exports.findAttr = (req, res, next) => {
    console.log('╠═[■]╦ findComponent');
    let connMsyl = mysql.createConnection(config);

    const query = `
        SELECT 
        equipment.vehicle_id,
        equipment.schema_id,
        equipment.data_value,
        schema_text.abbr_text,
        schema_text.full_text
        FROM
        jato.equipment
            INNER JOIN
        jato.schema_text ON jato.schema_text.schema_id =jato.equipment.schema_id
        WHERE
        equipment.vehicle_id = '${req.params.attributeId}'
            AND schema_text.language_id = '18'
        GROUP BY equipment.schema_id;
        `

    connMsyl.query(query, (err, results, fields) => {
        connMsyl.destroy()
        if (err) {
            console.log('║    ╚═[x]Error')
            next()
        }


        if (!results[0]) {
            console.log('║    ╚═[x]No encontre resultados')
            next()
        } else {
            console.log('║    ╚═[■]Si encontre resultados')
            req.attributesSAM = results
            // return res.send(req.attributesSAM)
            next()
        }
    });
}
//ir por la plantilla de d17 a la base de datos
exports.getTemplateSam = (req, res, next) => {
    console.log('getTemplateSam')
    dbAttributes.findOne({
        id: 'VERJATO'
    }, '', (err, doc) => {
        if (err) return next(err)
        req.attributeTM = doc
        delete req.attributeTM.__v
        // return res.send(doc)
        next()
    })
}

//asignar los valores a un nuevo JSON desde el doc de la consulta de attributos
exports.makeNewAtt = (req, res, next) => {
    console.log('makeNewAtt')
    req.newAtt = {}
    req.newAtt.attributes = {}
    req.newAtt.equipment = {}
    req.newAtt.equipment.comfort_group = {}
    req.newAtt.equipment.security_group = {}
    req.newAtt.equipment.electric_group = {}
    req.newAtt.equipment.gadgets_group = {}
    req.newAtt.equipment.extra_group = {}
    req.newAtt.equipment.security_group = {}
    req.newAtt.equipment.audio_group = {}
    req.newAtt.equipment.comfort_group.values = {}
    req.newAtt.equipment.security_group.values = {}
    req.newAtt.equipment.electric_group.values = {}
    req.newAtt.equipment.gadgets_group.values = {}
    req.newAtt.equipment.extra_group.values = {}
    req.newAtt.equipment.security_group.values = {}
    req.newAtt.equipment.audio_group.values = {}

    //si no trae atributos en SAM por default
    if (!req.attributesSAM) {
        next()
    }
    //Guardar los valores en un Json mas legible
    if (req.attributesSAM) {
        req.attributesSAM.forEach(e => {
            e.extId = e.schema_id
            /**Homologacion de attributos */
            if (e.extId == '901')
                req.newAtt.attributes.currencies = currencies(e.data_value)
            if (e.extId == '20602')
                req.newAtt.attributes.transmission = transmission(e.data_value)
            if (e.extId == '8702')
                req.newAtt.attributes.fuel = fuel(e.data_value)
            if (e.extId == '17402' || e.extId == '17403')
                req.newAtt.attributes.vesture = vestures(e.data_value)

            /**Homologacion de Equipamiento */
            let extValue = e.data_value
            let value = e.data_value == 'S' || e.data_value == 'Y' ? 'true' : 'false'

            if (e.extId == '5501')
                req.newAtt.equipment.comfort_group.values.vanity_mirror = value
            if (e.extId == '21701')
                req.newAtt.equipment.comfort_group.values.door_mirriors = value
            if (e.extId == '13001')
                req.newAtt.equipment.security_group.values.fog_lamps = value
            if (e.extId == '15701')
                req.newAtt.equipment.comfort_group.values.luggage_rack = value
            if (e.extId == '3101')
                req.newAtt.equipment.security_group.values.disc_brakes = value
            if (e.extId == '3201')
                req.newAtt.equipment.security_group.values.abs = value
            if (e.extId == '13801')
                req.newAtt.equipment.security_group.values.air_bag_courtain = value
            if (e.extId == '16301')
                req.newAtt.equipment.security_group.values.air_bag = value
            if (e.extId == '16401')
                req.newAtt.equipment.security_group.values.side_air_bags = value
            if (e.extId == '19401')
                req.newAtt.equipment.security_group.values.back_seat_air_bag = value
            if (e.extId == '21001')
                req.newAtt.equipment.comfort_group.values.air_conditioning = value
            if (e.extId == '21603')
                req.newAtt.equipment.electric_group.values.power_mirrors = value
            // if (e.extId == '6')
            //     req.newAtt.equipment.electric_group.values.power_door_locks = value
            if (e.extId == '23301')
                req.newAtt.equipment.electric_group.values.power_windows = value
            if (e.extId == '22501')
                req.newAtt.equipment.comfort_group.values.tinted_windows = value
            if (e.extId == '22305')
                req.newAtt.equipment.comfort_group.values.rear_defroster = value
            if (e.extId == '17811')
                req.newAtt.equipment.comfort_group.values.heated_seats = value
            if (e.extId == '5601')
                req.newAtt.equipment.comfort_group.values.parking_sensor = value
            if (e.extId == '6201')
                req.newAtt.equipment.comfort_group.values.automatic_door = value

            // if (e.extId == '4407')
            //     req.newAtt.equipment.comfort_group.values.wheel_positions = value
            if (e.extId == '18402')
                req.newAtt.equipment.comfort_group.values.leather_wheel = value
            if (e.extId == '18406')
                req.newAtt.equipment.comfort_group.values.wheel_positions = value
            //18407
            if (e.extId == '25201')
                req.newAtt.equipment.gadgets_group.values.navigation_system = value
            if (e.extId == '15601')
                req.newAtt.equipment.extra_group.values.sunroof = value
            // if (e.extId == '31')
            //     req.newAtt.equipment.security_group.values.alarm = value
            if (e.extId == '6503') {
                if (extValue == 'C' || extValue == 'F' || extValue == 'B' || extValue == 'P')
                    value = 'true'
                req.newAtt.equipment.extra_group.values.wheel_drive_4x4 = value
            }

            if (e.extId == '6901')
                req.newAtt.equipment.security_group.values.traction_control = value
            if (e.extId == '13401')
                req.newAtt.equipment.security_group.values.daytime_lights = value
            if (e.extId == '1327' || e.extId == '39101')
                req.newAtt.equipment.audio_group.values.cd_player = value
            if (e.extId == '38101') {
                req.newAtt.equipment.comfort_group.values.dvd_player = value
            }
            if (e.extId == '4501') {
                req.newAtt.equipment.comfort_group.values.cruise_control = value
            }
            if (e.extId == '4001') {
                req.newAtt.equipment.comfort_group.values.open_trunk_fuel = value
            }

            if (e.extId == '2201') {
                req.newAtt.equipment.extra_group.values.spoilers = value
            }
            if (e.extId == '32701')
                req.newAtt.equipment.comfort_group.values.car_entertaiment = value
            if (e.extId == '44801')
                req.newAtt.equipment.gadgets_group.values.bluetooth = value
            if (e.extId == '46906')
                req.newAtt.equipment.audio_group.values.auxiliary_input = value
            if (e.extId == '46907')
                req.newAtt.equipment.audio_group.values.usb_port = value
            if (e.extId == '21504')
                req.newAtt.equipment.security_group.values.parking_camera = value
        })
    }

    // return res.send({
    //     message: req.newAtt
    // })
    next()

}

/*
 req.attributeTM   ===> Plantilla para atributos D17
 req.attributesSAM ===> Los atributos traidos de SAM
 req.newAtt        ===> Es el nuevo JSON formado con los atributos traidos de SAM
 req.attibutesF    ===> Atributos Final
*/
//asignar los attributos obtenidos de la consulta en la plantilla d17
exports.armingJSON = (req, res) => {
    console.log('armingJSON')
    req.attributesFinal = {}
    let newAtribute = []
    let attribute = {}
    req.attributeTM.attributes.forEach(e => {
        if (e.valueType == 'group') {
            attribute = {
                id: e.id,
                oldId: e.oldId ? e.oldId : '',
                label: e.label ? e.label : '',
                help: e.help ? e.help : '',
                alert: e.help ? e.help : '',
                placeholder: e.placeholder ? e.placeholder : '',
                type: e.type ? e.type : '',
                inputType: e.inputType,
                valueType: e.valueType,
                required: e.required,
                groupId: e.groupId,
                allowEdit: e.allowEdit,
                order: e.order,
                value: []
            }
            e.values.forEach((b) => {
                attribute.value.push(parsenJson(b, req))
            })
            newAtribute.push(attribute)
        } else {
            newAtribute.push(parsenJson(e, req))
        }
    });

    req.attributesFinal = newAtribute
    newAtribute.push()

    let data = {
        id: req.attributeTM.id,
        categoryId: req.params.attributeId,
        category: req.attributeTM.category,
        attributes: newAtribute
    }
    req.attributesFinal = null
    req.attributesFinal = data

    return res.send(
        req.attributesFinal
    )
}
function parsenJson(b, req) {
    let condition = req.sellerSam.mainBrand.condition
    let aux = {
        id: b.id,
        oldId: b.oldId ? b.oldId : '',
        valueIdlabel: b.label ? b.label : '',
        type: b.type ? b.type : '',
        help: b.help ? b.help : '',
        alert: b.help ? b.help : '',
        placeholder: b.placeholder ? b.placeholder : '',
        inputType: b.inputType,
        valueType: b.valueType,
        required: b.required,
        groupId: b.groupId,
        groupName: nameGroups[b.groupId],
        allowEdit: b.allowEdit,
        order: b.order
    }
    /**
     * AQUI LAS CONDICIONES PARA SAM
     * >CONDITION nuevo o usado
     */
    if (b.valueId || b.valueId == '') {
        aux['valueId'] = b.valueId ? b.valueId : ''
        if (req.newAtt.attributes[b.oldId]) {
            console.log(req.newAtt.attributes[b.oldId])
            aux['valueId'] = req.newAtt.attributes[b.oldId]
        }
        aux['value'] = []
        b.values.forEach((d) => {
            aux.value.push({
                id: d.id,
                name: d.name
            })
        })
        if (b.id == 'condition') {
            if (condition == 'new') {
                console.log('**** es NUEVO')
                aux['value'] = [{
                    'id': 'CONDITION-NEW',
                    'name': 'Nuevo'
                }]
            }
        }
    } else {
        if (b.id == 'price') {
            aux['range'] = [
                {
                    "id": "condition",
                    "cases": [
                        {
                            "valueId": "CONDITION-NEW",
                            "min": 15000,
                            "max": 15000000
                        },
                        {
                            "valueId": "default",
                            "min": 15000,
                            "max": 15000000
                        }
                    ]
                }
            ]
        }
    }
    return aux
}
function fuel(data_value) {

    let Diesel = "FUEL-DIESEL"
    let ELECTRICO = "FUEL-ELECTRICO"
    let gas = "FUEL-GAS"
    let gasolina = "FUEL-GASOLINA"
    let HIBRIDO = "FUEL-HIBRIDO"

    let fuel = ''

    if (data_value == '1') fuel = gas
    if (data_value == '2') fuel = gas
    if (data_value == '3') fuel = gas
    if (data_value == '?') fuel = gasolina
    if (data_value == 'A') fuel = gas
    if (data_value == 'B') fuel = Diesel
    if (data_value == 'C') fuel = gasolina
    if (data_value == 'D') fuel = Diesel
    if (data_value == 'E') fuel = ELECTRICO
    if (data_value == 'F') fuel = gasolina
    if (data_value == 'G') fuel = gas
    if (data_value == 'H') fuel = gasolina
    if (data_value == 'L') fuel = gasolina
    if (data_value == 'M') fuel = gasolina
    if (data_value == 'N') fuel = gas
    if (data_value == 'P') fuel = gasolina
    if (data_value == 'R') fuel = ''
    if (data_value == 'T') fuel = gasolina
    if (data_value == 'U') fuel = gasolina

    return fuel
}

function transmission(data_value) {
    let transmission = ''
    if (data_value == '?') transmission = ''
    if (data_value == 'A') transmission = 'TRANS-AUTOMATICA'
    if (data_value == 'M') transmission = 'TRANS-MANUAL'
    return transmission
}

function vestures(data_value) {

    let tela = 'VESTURE-TELA'
    let piel = 'VESTURE-PIEL'
    let vinil = 'VESTURE-VINIL'
    let vestures = ''

    if (data_value == 'A') vestures = vinil
    if (data_value == 'C') vestures = tela
    if (data_value == 'L') vestures = piel
    if (data_value == 'S') vestures = vinil
    if (data_value == 'U') vestures = vinil
    if (data_value == 'V') vestures = vinil
    if (data_value == 'W') vestures = tela
    if (data_value == 'Y') vestures = piel

    return vestures
}

function currencies(data_value) {

    let mnx = 'CURRENCIE-MXN'
    let usd = 'CURRENCIE-USD'

    if (data_value == 'USD') mnx = usd
    return mnx
}