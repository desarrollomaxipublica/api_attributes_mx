'use strict'
const async = require('async')
const dbAttributes = require('../models/attributesMxp')
const request = require('request')
const base64url = require('base64url')

const nameGroups = {
	basic: 'Publicación',
	advanced: "Avanzado",
	business: 'Inteligencia de negocios',
	audio: 'audio',
	confort: 'Confort',
	documents: 'Documentos',
	electric: 'Eléctrico',
	extra: 'Extras',
	gadgets: 'Gadgets',
	security: 'Seguridad',
	interior: 'Exterior',
	exterior: 'Exterior',
	catalog: 'Catálogo'
}


function validTypeSeller(req, res, next) {
	if (req.token && req.token.type && req.token.type == 'group') {

		const url = 'http://d17.maxipublica.com/sellers/sellergroup/' + req.seller.groupKey
		request.get(url, (err, respon, body) => {
			if (err) cb(err, '')
			else {
				if (respon.statusCode === 200) {
					req.seller['sellerGroup'] = JSON.parse(body).sellers

					next()
				} else {
					next()
				}
			}
		})
	} else {
		next()
	}

}


function findPrice(req, res, next) {
	console.log('═══[ OK ]  Find Price')
	let murl = 'http://api.maxipublica.com/prices/' + req.params.attributeId + '?token=' + req.query.token
	request.get(
		murl, {
			timeout: 1500
		}, (err, resp, body) => {
			if (!err) {
				body = JSON.parse(body)
				req.price = body.salePrice == 0 ? -1 : body.salePrice
				req.currency = body.currency
				next()
			} else {
				req.price = -1
				req.currency = "MXN"
				next()
			}
		})
}

function findAttribute(req, res, next) {
	console.log('═══[ OK ]  Find Attribute')
	dbAttributes.findOne({
		id: req.params.attributeId
	}, (err, doc) => {
		//console.log('attribbutt econtrado',err, doc, req.params.attributeId)
		if (!err) {
			if (doc) {
				req.data = doc
				delete req.data.__v
				next()
			} else {
				next()
			}
		} else {
			res
				.status(500)
				.send({
					'status': 500,
					message: err
				}).status(500)
		}
	})
}

function loadDeafault(req, res, next) {
	//Carga la plantilla por default en caso de no encontrar la categoria identificada
	console.log('═══[ OK ]  Loading template for default')
	let def = 'v_1'
	if (/v_1/.test(req.params.attributeId)) {
		console.log(`   ╚═══[ It´s vehicles ]`)
		def = 'v_1'
	} else if (/v_2/.test(req.params.attributeId)) {
		console.log(`   ╚═══[ It´s trucks ]`)
		def = 'v_2'
		req.OriginBrand = 'v_2'
	}


	if (!req.data) {
		console.log(`      ╚═══[ search ]`)
		dbAttributes.findOne({
			id: def
		}, (err, doc) => {
			if (!err) {
				if (doc) {
					console.log(`         ╚═══[ FOUND ]`)
					req.data = doc

					delete req.data.__v
					next()
				} else {
					console.log(`         ╚═══[ NOT FOUND ]`)
					res
						.status(409)
						.send({
							status: 409,
							message: 'The attribute with id ' + req.params.attributeId + ' not exist'
						}).status(404)
				}
			} else {
				res
					.status(500)
					.send({
						'status': 500,
						message: err
					}).status(500)
			}
		})
	} else {
		console.log(`      ╚═══[ not search ]`)
		next()
	}
}

function getJson(req, res, next) {
	//Acomodamos las salidas  solo para los registros que existan y con el campo value
	console.log('═══[ OK ]  Get JSON')
	let newAtribute = []
	let attribute = {}
	let subGrup = []
	let order = 0
	let orderGroup = 0
	req.data.attributes.forEach((e) => {

		if (req.frontV && req.frontV === 'kiamx') {
			if (e.id == 'vin') {
				e.required = true
			}
			if (e.id == 'badge') {
				if (req.brandQ && req.brandQ == 'kia') {
					e.required = true
					e.label = "Garantía"
					e.valueId = 'BADGE-KIA-ANY'
				}
			}
			if (e.id == 'energy') {
				if (req.brandQ && req.brandQ == 'kia') {
					e.required = true
					e.allowEdit = false
					e.valueId = 'FUEL-GASOLINA'
				}
			}
			if (e.id == 'transmission') {
				if (req.brandQ && req.brandQ == 'kia') {
					e.required = true
					e.allowEdit = false
					e.valueId = ''
				}
			}
		} else {
			if (e.id == 'transmission') {
				e.allowEdit = true
			}
		}
		if (e.id == 'condition') {
			e.allowEdit = true
		}
		if (e.id == 'direction') {
			e.allowEdit = true
		}
		if (e.id == 'vin' || e.id == 'numberPlate' || e.id == 'stock') {//////Aqui 1
			e.inputType = 'text'
		}

		// if (e.id == 'loadingCapacity') {//////Aqui 1
		// 	console.log(e)
		// 	e.regex='Expresion'
		// }

		if(e.id == 'priceGroup'){
			e.type='attribute'
		}
		if(e.id == 'odometerGroup'){
			e.type='attribute',
			e.groupName = ''
		}
		if (e.valueType == 'group') {
			attribute = {
				id: e.id,
				oldId: e.oldId ? e.oldId : '',
				label: e.label ? e.label : '',
				help: e.help ? e.help : '',
				alert: e.help ? e.help : '',
				placeholder: e.placeholder ? e.placeholder : '',
				type: e.type ? e.type : '',
				inputType: e.inputType,
				valueType: e.valueType,
				required: e.required,
				groupId: e.groupId,
				groupName: nameGroups[e.groupId],
				allowEdit: e.allowEdit,
				order: e.order,
				value: []
			}

			e.values.forEach((b) => {
				if (req.seller.attributesRequired.indexOf(b.id) >= 0) {
					b['required'] = true
				}
				attribute.value.push(parsenJson(b, req))
			})
			newAtribute.push(attribute)
		} else {
			if (req.seller.attributesRequired.indexOf(e.id) >= 0) {
				e['required'] = true
			}
			newAtribute.push(parsenJson(e, req))
		}
		order += 1
	})



	req.data.attributes = newAtribute
	newAtribute.push()

	let data = {
		id: req.data.id,
		categoryId: req.data.categoryId,
		category: req.data.category,
		attributes: newAtribute
	}
	req.data = null
	req.data = data
	next()
}

function parsenJson(b, req) {
	let precio = req.price
	let app = req.query.app
	let seller = req.seller

	let aux = {
		id: b.id,
		oldId: b.oldId ? b.oldId : '',
		label: b.label ? b.label : '',
		type: b.type ? b.type : '',
		help: b.help ? b.help : '',
		alert: b.help ? b.help : '',
		placeholder: b.placeholder ? b.placeholder : '',
		inputType: b.inputType,
		valueType: b.valueType,
		required: b.required,
		groupId: b.groupId,
		groupName: nameGroups[b.groupId],
		allowEdit: b.allowEdit,
		order: b.order
	}
	if (b.id == 'finantial' || b.id == 'waranty') {
		aux['inputType'] = 'hidden'
	}
	if (b.id == 'bodyType') {
		aux['label'] = "Segmento"
	}
	if (b.id === 'vin'){
		aux['regex'] = '[0-9A-Z]{18}|^$'
	}
	if(b.id == 'loadingCapacity'){
		aux['regex'] = '/^\d{1,5}$/'
	}
	if(b.id == 'ballastCapacity'){
		aux['regex'] = '/^\d{1,5}$/'
	}
	if (b.id == 'title'){
		aux['rules'] = b.rules
		aux['regex'] = '/^[A-Z]+[A-Za-zñÑ ]{3,59}$/'
	}
	if (b.id == 'stock'){
		aux['rules'] = b.rules
		aux['regex'] = '/^[0-9]$/'
	}
	if (b.id === 'odometer') {
		//aux['inputType'] = 'number'
		aux['range'] = [
			{
				"id": "condition",
				"cases": [
					{
						"valueId": "CONDITION-NEW",
						"min": 1,
						"max": 100
					},
					{
						"valueId": "CONDITION-USED",
						"min": 100,
						"max": 999999
					},
					{
						"valueId": "default",
						"min": 100,
						"max": 999999
					}
				]
			}
		]
		if (req.frontV == 'kiamx') {
			if (req.brandQ == 'kia') {
				aux['range'] = [
					{
						"id": "condition",
						"cases": [
							{
								"valueId": "CONDITION-NEW",
								"min": 1,
								"max": 100
							},
							{
								"valueId": "CONDITION-USED",
								"min": 500,
								"max": 999999
							},
							{
								"valueId": "default",
								"min": 100,
								"max": 999999
							}
						]
					}
				]
			}
		}
	}
	if (b.valueId || b.valueId == '') {
		aux['valueId'] = b.valueId ? b.valueId : ''
		aux['value'] = []
		b.values.forEach((d) => {
			aux.value.push({
				id: d.id,
				name: d.name
			})
		})
		if (b.id == 'energy') {
			if (req.brandQ && req.brandQ == 'kia') {
				aux['value'] = []
				aux.value.push({
					id: "FUEL-GASOLINA",
					name: "Gasolina"
				})
			}
		}


	} else {
		if (b.id == 'price') {
			aux['range'] = [
				{
					"id": "condition",
					"cases": [
						{
							"valueId": "CONDITION-NEW",
							"min": 15000,
							"max": 15000000
						},
						{
							"valueId": "default",
							"min": 15000,
							"max": 15000000
						}
					]
				}
			]

			if (precio !== -1 && precio !== 1) {

				aux['placeholder'] = app ? '' : '$' + number_format(precio) + ' precio sugerido'
				aux['help'] = '$' + number_format(precio) + ' precio sugerido'

				aux['value'] = precio
				if (req.frontV == 'kiamx') {
					if (req.brandQ == 'kia') {
						aux['range'] = [
							{
								"id": "condition",
								"cases": [
									{
										"valueId": "CONDITION-NEW",
										"min": 15000,
										"max": Math.round(precio * 1.1)
									},
									{
										"valueId": "default",
										"min": 15000,
										"max": 15000000
									}
								]
							}
						]
					}
				}
			} else {
				//aux['placeholder'] = app ? '' : '$' + number_format(precio) + ' precio sugerido'
				aux['placeholder'] = ''
				aux['help'] = 'No hay precio sugerido'

				aux['value'] = -1
			}
		} else {
			aux['value'] = b.value === 'false' ? false : b.value === 'true' ? true : b.value

			if (aux.valueType === 'number') {
				aux.value = parseInt(aux.value)

				if (isNaN(aux.value)) {
					aux.value = -1
				}
			}
		}
	}



	return aux
}

function loadValuesDefault(req, res, next) {
	console.log('═══[ OK ]  Load Values Default')
	let badSeller = []
	//si badges existe y si frontVersion es diferente de kia

	if (req.seller.badges && req.frontV != 'kiamx') {
		console.log(`   ╚═══[ Badges Exist ]`)
		Object.keys(req.seller.badges).forEach((e) => {
			// console.log(e)
			if (req.seller.badges[e].status && req.seller.badges[e].status == 'active') {
				badSeller.push({
					id: e,
					name: req.seller.badges[e].text
				})
			}
		})
	} else {
		console.log(`   ╚═══[ Badges Not Exist ]`)
		req.data.attributes.forEach((b) => {
			if (b.id == 'badge' && req.brandQ == 'kia') {
				// console.log('********************', b.id)
				b.value = [
					{
						_id: undefined,
						id: "BADGE-KIA-ANY",
						name: "Ninguno"
					},
					{
						_id: undefined,
						id: "BADGE-KIA-VIGENTE-FABRICA",
						name: "Vigente de fábrica"
					},
					{
						_id: undefined,
						id: "BADGE-KIA-CONFIDENCE",
						name: "Confidence"
					},
					{
						_id: undefined,
						id: "BADGE-KIA-FABRICA",
						name: "KIA de fábrica"
					},
					{
						_id: undefined,
						id: "BADGE-KIA-NO",
						name: "NO"
					}
				]
			}
		})
	}

	req.seller.valuesDefault.forEach((e) => {
		req.data.attributes.forEach((b) => {
			if (b.valueType == 'group') {
				b.value.forEach((c) => {
					if (c.id === e.id) {
						//console.log('<<<<<<<<<<<<<<<', e.id, c.id, e.value)
						if (c.valueId || c.valueId === "") {
							c['valueId'] = e.value
						} else {
							c['value'] = e.value
						}
					}
				})
				if (badSeller.length > 0) {
					//console.log('********************', b.id)
					//b.value = badSeller
					//badSeller = []
				}
			} else {
				if (b.id == e.id) {
					//console.log('>>>>>>>>>>>>>>>>', e.id, b.id, e.value)
					if (b.valueId || b.valueId === "") {
						b['valueId'] = e.value
						//console.log(`ValueID${b['valueId']}`)
					} else {
						b['value'] = e.value
						//console.log(`ValueID${b['value']}`)
					}

					if (b.id === 'title') {
						b['rules'] = e.rules ? e.rules : b.rules
					}

					//b.id ==='badge' && badSeller.length>0
					//console.log(`(XXXX) ${b.id}`)

					if (b.id === 'badge' && badSeller.length > 0) {
						// console.log(`>>> ${b.id} igual de BADGE`)

						b.value = badSeller
						//badSeller = []
					}
				}
				//console.log('B',b.value)
			}
			if (b.id == 'badge' && badSeller.length > 0) {
				b.value = badSeller
				//badSeller = []
			}
		})
		if (e.id == 'badge') {
		}
		if (e.id == 'energy') {
			// console.log('ENERGY', e.id)
		}
	})

	if (req.query.app) {
		req.data.attributes.forEach((b) => {

			switch (b.valueType) {
				case "group":
					// b.value = b.value;
					b.value.forEach(e => {
						switch (e.inputType) {
							case "text":
								//e.valueText = e.value;//Aqui meter las condiciones
								switch (e.valueType) {
									case "text":
										e.valueText = e.value;
										break;
									case "enum":
										e.valueEnum = e.value;
										break;
									case "hidden":
										e.valueHidden = e.value;
										break;
									case "objectId":
										e.valueObjectId = e.value;
										break;
									case "number":
										e.valueNumber = e.value;
										break;
								}
								break;
							case "enum":
								e.valueEnum = e.value;
								break;
							case "hidden":
								e.valueHidden = e.value;
								break;
							case "number":
								e.valueNumber = e.value;
								break;
							case "oejectId":
								e.valueObjectId = e.value;
								break;
						}
						e.value = undefined
					});
					break;
				case "text":
					switch (b.inputType) {
						case "text":
							b.valueText = b.value;
							break;
						case "enum":
							b.valueEnum = b.value;
							break;
						case "hidden":
							b.valueHidden = b.value;
							break;
						case "objectId":
							b.valueObjectId = b.value;
							break;
						case "number":
							b.valueNumber = b.value;
							break;
					}
					break;
				case "number":
					b.valueNumber = b.value;
					break;
				case "boolean":
					b.valueBoolean = b.value;
					break;
				case "textRules":
					b.valueText = b.value;
					break;
			}
			if (b.inputType == 'enum' && b.value == '') {
				// console.log(b.id)
				b.valueId = ''
				b.valueEnum = []
				// console.log('b.value: ', b.value);
			}
			if (b.valueType !== 'group')
				b.value = undefined

		})
	}


	next()
}

function builApp(req, res, next) {

}

function buildAttrbutesExtra(req, res, next) {
	console.log('═══[ OK ]  Build Attributes Extra')
	let extra = req.seller.attributesExtra && req.seller.attributesExtra[req.categoryId] || {}
	//console.log('extra: ', extra);

	let odometerSuffix = {
		"id": "odometerSuffix",
		"oldId": "",
		"label": "Sufijo odómetro",
		"type": "attribute",
		"help": "",
		"alert": "",
		"placeholder": "Aprox.",
		"inputType": "text",
		"valueType": "text",
		"required": false,
		"groupId": "basic",
		"groupName": "Publicación",
		"allowEdit": true,
		"order": 0,
	}

	if (req.query.app) {
		// console.log('APP => TRUE')
		if (req.OriginBrand && req.OriginBrand == 'v_2') {
			if (extra.odometerSuffix) {
				odometerSuffix["valueText"] = extra.odometerSuffix.value != '' ? extra.odometerSuffix.value : ""
			} else {
				console.log('   ╚═══[ Not find odometerSuffix ]')
			}
		}
	} else {
		// console.log('APP => FALSE')
		if (req.OriginBrand && req.OriginBrand == 'v_2') {
			if (extra.odometerSuffix) {
				odometerSuffix["value"] = extra.odometerSuffix.value != '' ? extra.odometerSuffix.value : ""
			} else {
				console.log('   ╚═══[ Not find odometerSuffix ]')
				odometerSuffix["value"] = ''
			}
		}
	}

	if (req.seller.attributesExtra && req.seller.attributesExtra.groupID && req.seller.attributesExtra.groupID == 7) {
		console.log(`
╔═════════════════════════════════╗
    ODOMETER SUFFIX BY GROUP ${req.seller.attributesExtra.groupID}        
╚═════════════════════════════════╝`)
		req.data.attributes[2].value.push(odometerSuffix)
	}else if (extra && extra.odometerSuffix) {
		req.data.attributes[2].value.push(odometerSuffix)

	}





	// if (extra && extra.title)
	// req.data.attributes.unshift(title)
	//console.log('req.data.attributes[2]: ', req.data.attributes[2]);



	next()
}


function viewJson(req, res, next) {

	res
		.status(200)
		.send(req.data).status(200)
}

function number_format(amount, decimals) {

	amount += ''; // por si pasan un numero en vez de un string
	amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto
	decimals = decimals || 0; // por si la variable no fue fue pasada
	// si no es un numero o es igual a cero retorno el mismo cero
	if (isNaN(amount) || amount === 0)
		return parseFloat(0).toFixed(decimals);

	// si es mayor o menor que cero retorno el valor formateado como numero
	amount = '' + amount.toFixed(decimals);
	var amount_parts = amount.split('.'),
		regexp = /(\d+)(\d{3})/;
	while (regexp.test(amount_parts[0]))
		amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');

	return amount_parts.join('.');
}

module.exports.findAttribute = findAttribute
module.exports.loadDeafault = loadDeafault
module.exports.getJson = getJson
module.exports.loadValuesDefault = loadValuesDefault
module.exports.viewJson = viewJson
module.exports.findPrice = findPrice
module.exports.buildAttrbutesExtra = buildAttrbutesExtra
module.exports.validTypeSeller = validTypeSeller