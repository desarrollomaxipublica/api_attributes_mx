'use strict'

module.exports.error = (res, name, data) => {
  data = data ? data : ''
  let mError = {
    internalServerError: {
      status: 500,
      name: 'internalServerError',
      message: 'Internal Server Error' +(data ? ': '+ data: ''),
      customMessage: 'Error Interno en el servidor'
    },
    tokenNotFound: {
      status: 401,
      name: 'tokenNotFound',
      message: 'token access not found',
      customMessage: 'El token es requerido'
    },
    notFoundField: {
      status: 400,
      name: 'notFoundField',
      message: 'Fields are required ' + data,
      customMessage: 'Los campos son requeridos: ' + data
    },
    siteIdNotFound: {
      status: 400,
      name: 'siteIdNotFound',
      message: 'The siteId not exist',
      customMessage: 'El sitio ' + (data ? data : '') + ' no existe'
    },
    alreadyRegistered: {
      status: 409,
      name: 'alreadyRegistred',
      message: 'the ' + (data ? data : 'data') + ' already has registered',
      customMessage: 'El ' + (data ? data : 'dato') + ' ya está registrado'
    },
    notRegistered: {
      status: 404,
      name: 'notRegistered',
      message: 'the ' + (data ? data : 'data') + ' not has registered',
      customMessage: 'El ' + (data ? data : 'dato') + ' no está registrado'
    },
    notAllowed: {
      status: 409,
      name: 'notAllowed',
      message: 'You do not have permissions',
      customMessage: 'No tienes permisos para acceder al recurso'
    },
    invalidValues: {
      status: 400,
      name: 'invalidValues',
      message: 'Invalid packet values',
      customMessage: 'Los valores para los paquetes no  son inválidos'
    },
    invalidDate: {
      status: 400,
      name: 'invalidDate',
      message: 'Invalid Date',
      customMessage: 'La fecha que ingresó es inválida'
    },
    invalidDateExpiration: {
      status: 400,
      name: 'invalidDateExpiration',
      message: 'Invalid Date Expiration',
      customMessage: 'La fecha  de expiración en inválida'
    },
    sumValuesInvalid: {
      status: 400,
      name: 'sumValuesInvalid',
      message: 'The sum of the values is invalid',
      customMessage: 'La suma de los valores es inválida'
    },
    invalidPackId: {
      status: 400,
      name: 'invalidPackId',
      message: 'The packId not exists',
      customMessage: 'El identificador del paquete no existe'
    },

    errorApiGmail: {
      status: 500,
      name: 'errorApiGmail',
      message: data,
      customMessage: 'Error al hacer peticion a la api de Gmail'
    },
    notFound: {
      status: 404,
      name: 'notFound',
      message: 'Not found access',
      customMessage: 'No se encuentran los accesos'
    },
    noMessages: {
      status: 200,
      name: 'noMessages',
      message: 'historyId has not messages',
      customMessage: 'No hay mensajes para leer'
    }


  }
  if (mError[name]) {
    res
      .status(mError[name].status)
      .send(mError[name])
  } else {
    res
      .status(mError['internalServerError'].status)
      .send(mError['internalServerError'])
  }
}

module.exports.success = (res, name, data) => {
  let success = {
    saved: {
      status: 200,
      name: 'saved',
      message: 'Data was successfully saved!',
      customMessage: 'El ' + (data ? data : 'dato') + ' se registró con éxito'
    },
    deleted: {
      status: 200,
      name: 'deleted',
      message: 'Data was successfully deleted!',
      customMessage: 'El ' + (data ? data : 'dato') + ' se eliminó con éxito'
    }

  }
  if (success[name]) {
    res
      .status(success[name].status)
      .send(success[name])
  } else {
    res
      .status(success['saved'].status)
      .send(success['saved'])
  }
}
module.exports.custom = (res, name, data) => {
  let customSms = {
    tokenInvalidMlm: {
      status: 400,
      name: 'tokenInvalidMlm',
      message: 'Invalid access_token of  Mlm',
      customMessage: 'El access_token de Mercado Libre es inválido'
    }
  }
  if (customSms[name]) {
    res
      .status(customSms[name].status)
      .send(customSms[name])
  }
}