const request = require('request')
const base64url = require('base64url')
const error = require('./customRes')




module.exports.getTokenMxp = (req, res, next) => {

    let url = 'http://d17.maxipublica.com/oauth/'
    let access = {
        "email": "",
        "password": ""
    }
    request.post({
        url: url,
        body: access,
        json: true
    }, function (err, respon, body) {
        if (!err)
            if (body) {
                try {
                    req.query.token = JSON.parse(body).token
                    next()
                } catch (err) {
                    console.log(err, body)
                    error.error(res, 'internalServerError')
                }
            } else {
                console.log(err, body)
                error.error(res, 'internalServerError')
            }
        else {
            console.log(err, body)
            error.error(res, 'internalServerError')
        }
    })
}

module.exports.validToken = (req, res, next) => {
    if (req.query.token) {
        let url = 'http://d17.maxipublica.com/oauth/valid/' + req.query.token
        console.log(url)
        request.get({
            url: url
        }, (err, response, body) => {
            console.log(response.statusCode)
            if (!err) {
                if (response.statusCode == 200) {
                    console.log(body)
                    body = JSON.parse(body)
                    req.token = {}
                    req.token.userId = base64url.decode(body.userId)
                    req.token.sellerId = base64url.decode(body.sellerId)
                    req.token.rol = base64url.decode(body.rol)
                    req.token.type = base64url.decode(body.type)
                    console.log(req.token)
                    if ((req.token.type === 'admin' && req.token.rol === 'admin') || req.token.type === 'seller' || req.token.type === 'group') {
                        next()
                    } else {
                        error.error(res, 'notAllowed')
                    }
                } else {
                    res
                        .status(response.statusCode)
                        .send(JSON.parse(body))
                }
            } else {
                error.error(res, 'internalServerError')
            }
        })
    } else {
        error.error(res, 'tokenNotFound')
    }

}