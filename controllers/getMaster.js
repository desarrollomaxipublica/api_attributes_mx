//Obtiene la plantilla en objetos para ser la validadción de la api de ads
'use strict'
var async = require('async'),
	dbAttributes = require('../models/attributesMxp'),
	request = require('request'),
	base64url = require('base64url')



function findAttribute(req, res, next) {
	console.log('(*_*) findAttribute')
	dbAttributes.findOne({
		id: req.params.attributeId
	}, (err, doc) => {
		//console.log('attribbutt econtrado',err, doc, req.params.attributeId)
		if (!err) {
			if (doc) {
				req.data = doc
				delete req.data.__v
				next()
			} else {
				next()
			}
		} else {
			res
				.status(500)
				.send({
					'status': 500,
					message: err
				}).status(500)
		}
	})
}

function loadDeafault(req, res, next) {
	//Carga la plantilla por default en caso de no encontrar la categoria identificada
	console.log('cargando por default')

	let def = 'v_1'
	if (/v_1/.test(req.params.attributeId)) {
		console.log('viene de autos')
		def = 'v_1'
	} else if (/v_2/.test(req.params.attributeId)) {
		console.log('viene de camiones')
		def = 'v_2'
	}


	if (!req.data) {
		console.log('******************* buscare')
		dbAttributes.findOne({
			id: def
		}, (err, doc) => {
			if (!err) {
				if (doc) {
					console.log('encontre')
					req.data = doc
					delete req.data.__v
					next()
				} else {
					console.log('no encontre')
					res
						.status(409)
						.send({
							status: 409,
							message: 'The attribute with id ' + req.params.attributeId + ' not exist'
						}).status(200)
				}
			} else {
				res
					.status(500)
					.send({
						'status': 500,
						message: err
					}).status(500)
			}
		})
	} else {
		next()
	}
}

function getJson(req, res, next) {
	console.log('Acomodamos las salidas  solo para los registros que existan y con el campo value')
	//Acomodamos las salidas  solo para los registros que existan y con el campo value

	let newAtribute = []
	let attribute = {}
	let subGrup = []
	let order = 0
	let orderGroup = 0


	req.data.attributes.forEach((e) => {


		if (e.valueType == 'group') {
			e.values.forEach((b) => {
				console.log(`(º_º) E.id ${e.id}`)
				if (req.seller.attributesRequired.indexOf(b.id) >= 0) {
					b['required'] = true
				}
				console.log(`(º_º) B.id ${b.id}`)
				attribute[b.id] = parsenJson(b)
			})

		} else {
			if (req.seller.attributesRequired.indexOf(e.id) >= 0) {
				e['required'] = true
			}
			if (e.id !== 'finantial' && e.id !== 'waranty') {
				attribute[e.id] = parsenJson(e)
			}

		}
		order += 1
	})

	req.data.attributes = newAtribute
	//res.send(newAtribute)
	let data = {
		id: req.data.id,
		categoryId: req.data.categoryId,
		category: req.data.category,
		attributes: attribute
	}
	req.data = null
	req.data = data
	//console.log(`(º_º) req.data ${JSON.stringify(req.data)}`)
	next()
	// res.send(req.data)
}

function parsenJson(b) {


	let aux = {
		id: b.id,
		label: b.label ? b.label : '',
		type: b.type ? b.type : '',
		groupId: b.groupId,
		order: b.order
	}



	if (b.valueId || b.valueId == '') {
		aux['valueId'] = b.valueId
		aux['value'] = []
		b.values.forEach((d) => {
			aux.value.push({
				id: d.id,
				name: d.name
			})
		})
	} else {

		aux['value'] = b.value === 'false' ? false : b.value === 'true' ? true : b.value

		if (aux.valueType === 'number') {


			aux.value = parseInt(aux.value)

			if (isNaN(aux.value)) {

				aux.value = -1
			}
		}


	}

	return aux
}

function loadValuesDefault(req, res, next) {
	console.log('ValuesDefault', req.seller.valuesDefault)
	req.seller.valuesDefault.forEach((e) => {

		if (req.data.attributes[e.id]) {
			console.log(`VALUEID ${req.data.attributes[e.id].value}`)
			if (req.data.attributes[e.id].valueId || req.data.attributes[e.id].valueId === "") {
				console.log(`(-_-) req.data.attr[].valueID  ${req.data.attributes[e.id].valueId}`)
				req.data.attributes[e.id]['valueId'] = e.value
			} else {
				req.data.attributes[e.id]['value'] = e.value
			}
		}

	})
	//My Code by Badges
	if (req.data.attributes) {
		if (req.data.attributes.badge) {
			//si estan meter los que vienen de la DB
			console.log(`======================> DEFAULT BADGE EXIST`)
			//console.log(req.attr)
			//console.log(req.data.attributes.badge.value)
			let badSellerMaster = []
			if (req.attr) {
				if(req.attr.badges){
					console.log(req.attr)
					console.log('==============> PERSONAL BADGES EXIST',req.attr)
					Object.keys(req.attr.badges).forEach((e) => {
						if (req.attr.badges[e].status && req.attr.badges[e].status == 'active') {
							console.log(`${e} is ACTIVE`)
							badSellerMaster.push({
								id: e,
								name: req.attr.badges[e].text
							})
						}
					})
					req.data.attributes.badge.value = badSellerMaster
				}else{
					console.log('==============> PERSONAL BADGES NOT EXIST ASIGNATE DEFAULT BADGE')
				}
			}
			console.log('(*_*) END')
			
			//console.log(req.data.attributes.badge.value)
		}
	}
	//End

	next()
}
function buildAttrbutesExtra(req, res, next) {
	console.log('buildAttrbutesExtra: ', 'buildAttrbutesExtra', req.categoryId);
	let extra = req.seller.attributesExtra && req.seller.attributesExtra[req.categoryId] || {}
	console.log('extra: ', extra);
	let title = {
		"id": "title",
		"label": "Título",
		"type": "attribute",
		"groupId": "basic",
		"order": 0
	}

	let odometerSuffix = {
		"id": "odometerSuffix",
		"type": "attribute",
		"groupId": "basic",
		"order": 4
	}

	if (req.query.app) {
		title["valueText"] = extra.title && extra.title.value || ""
		odometerSuffix["valueText"] = extra.odometerSuffix && extra.odometerSuffix.rules || ""
	} else {
		title['value'] = ""
		odometerSuffix["value"] = ""
	}


	if (extra && extra.title)
		req.data.attributes['title'] = title

	if (extra && extra.odometerSuffix) {
		const index = extra.title ? 2 : 1
		req.data.attributes['odometerSuffix'] = odometerSuffix
	}

	next()
}


function viewJson(req, res, next) {
	res
		.status(200)
		.send(req.data).status(200)
}

function number_format(amount, decimals) {

	amount += ''; // por si pasan un numero en vez de un string
	amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto
	decimals = decimals || 0; // por si la variable no fue fue pasada
	// si no es un numero o es igual a cero retorno el mismo cero
	if (isNaN(amount) || amount === 0)
		return parseFloat(0).toFixed(decimals);

	// si es mayor o menor que cero retorno el valor formateado como numero
	amount = '' + amount.toFixed(decimals);
	var amount_parts = amount.split('.'),
		regexp = /(\d+)(\d{3})/;
	while (regexp.test(amount_parts[0]))
		amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');

	return amount_parts.join('.');
}

module.exports.findAttribute = findAttribute
module.exports.loadDeafault = loadDeafault
module.exports.getJson = getJson
module.exports.loadValuesDefault = loadValuesDefault
module.exports.buildAttrbutesExtra = buildAttrbutesExtra
module.exports.viewJson = viewJson