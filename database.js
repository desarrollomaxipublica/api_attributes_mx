'use strict';

var mongoose = require('mongoose')
const autoIncrement = require('mongoose-auto-increment');
const config = require('./config')


var connection = mongoose.connect(config.maxipublicaMx, {
  useMongoClient: true
}, function (err) {
  if (err)
    console.log('Unable to connect to database');

  console.log('Connection to databse succesful');
});

autoIncrement.initialize(connection);